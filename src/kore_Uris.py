# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2019-03-04 16:21:58
# @Last Modified by:   vu
# @Last Modified time: 2019-04-02 15:14:23


ERRORS = {
    'getUris': 'entities not in res',
    'saveUris': 'uris not in res'
}
'''
get list entities of IT companies from KORE_entity_relatedness
'''
import re, json

def __getEntities(file_location):
    with open(file_location, encoding='utf-8') as f:
        entities = [line.strip() for line in f]

    seen = set()
    unique_entities = [x for x in entities if x not in seen and not seen.add(x)]

    return unique_entities, entities


def getEntities(res, params):
    file_location = params['kore_file_location']
    uniq_en, entities = __getEntities(file_location)
    res = {
        'unique_entities': uniq_en,
        'entities': entities
    }
    return res


'''
get uris from dbpedia by using wordsToUris
'''
from wordsToUris import WordsToUris

def __getUris(entities, endpoint, service, delay=0.1):
    w2i = WordsToUris()
    w2i.setQueryPoint(endpoint)
    uris = w2i.wordsToUris(entities, service=service, delay=delay)
    return uris


def getUris(res, params):
    if 'entities' not in res: raise Exception(ERRORS['getUris'])
    uris = __getUris(
        res['entities'],
        endpoint = params['endpoint'],
        service = params['service'],
        delay = params['delay'] if 'delay' in params else 0.1
    )
    res.update({'uris': uris})
    return res


'''
save uris to files
params: filename (str), dir (str), unique_entities (list, optional)
'''
import os, json

def __saveUris(uris, params):
    if not os.path.isdir(params['dir']): os.mkdir(params['dir'])

    # save uris to json file
    file_location = os.path.join(params['dir'], params['filename'])
    with open(file_location, 'w', encoding = 'utf-8') as fp:
        json.dump(uris, fp)

    # save entities who missed uri to a text file
    if 'unique_entities' not in params: return
    missed_uris = [x for x in params['unique_entities'] if x not in uris]
    if missed_uris:
        fname = '{}_missed_uris.txt'.format(os.path.splitext(params['filename'])[0])
        file_location = os.path.join(params['dir'], fname)
        if os.path.isfile(file_location): os.remove(file_location)
        for entity in missed_uris:
            print(entity, file=open(file_location, 'a'))


def saveUris(res, params):
    if 'uris' not in res: raise Exception(ERRORS['saveUris'])
    if 'unique_entities' in res: params['unique_entities'] = res['unique_entities']
    __saveUris(res['uris'], params)
    return res


'''
read uris from json file
'''
import os, json

def __readUris(params):
    file_location = os.path.join(params['dir'], params['filename'])
    with open(file_location) as fp: jdata = json.load(fp)
    uris = list(jdata.values()); uris.sort()
    return uris


def readUris(res, params):
    uris = __readUris(params)
    res = {'uris': uris}
    return res


'''
spearman similarity
'''
from scipy.stats import spearmanr
import json, re
from itertools import groupby

def __getRankings__(x):
    """Summary
    Get ranking of elements in a list x of tuple (uri, score) based on score

    Args:
        x (LIST): list of tuple (uri, score)

    Returns:
        DICT: dict of couple {uri: ranking} {'u1': r1, 'u2': r2, etc.}
    """
    x.sort(key=lambda x: x[1], reverse = True)
    rankings = dict()
    rank = 1
    for k, v in groupby(x, lambda x: x[1]):
        grp = [{tup[0]: rank} for tup in v]
        for d in grp: rankings.update(d)
        rank += len(grp)
    return rankings


def __getKoreEntities__(kore_file_location):
    """Summary
    Get list of KORE entities of a category in form
    entities[main_entity] = sub_entities
    where sub_entities is a list

    Args:
        kore_file_location (str): file path to KORE entities of a category

    Returns:
        LIST: list of kore entities
    """
    entities = dict()
    main_entity = None
    sub_entities = list()
    with open(kore_file_location, encoding='utf-8') as f:
        for line in f:
            if not re.match(r'[ \t]', line):
                if main_entity:
                    entities.update({main_entity: sub_entities})
                main_entity = line.strip()
                sub_entities = list()
            else:
                sub_entities.append(line.strip())
        if main_entity and main_entity not in entities:
            entities.update({main_entity: sub_entities})
    return entities


def getSpearmanrSimilarity(prev_res, params):
    """Summary
    Compute spearmanr similarity

    Args:
        prev_res (dict): previous result
                         obligatory key(s): model
        params (dict): keys: kore_file_location, w2i_file_location
            - kore_file_location (str): file path to KORE entities
            - w2i_file_location (str): file path to word to uri json file

    Returns:
        DICT: previous model with updated key 'spearmanr'
    """
    model = prev_res['model']
    kore_file_location = params['kore_file_location']
    w2i_file_location = params['w2i_file_location']
    so_namespaces = params['so_namespaces'] if 'so_namespaces' in params else dict()

    with open(w2i_file_location) as f: jdata = json.load(f)
    entities = __getKoreEntities__(kore_file_location)

    predicted = list(); gold = list()
    for main_entity in entities.keys():
        if not so_namespaces:
            me_uri = jdata[main_entity]
            sub_entities = entities[main_entity]
            x = [(jdata[sub_en], model.wv.similarity(me_uri, jdata[sub_en])) for sub_en in sub_entities]
        else:
            ns_me_uri = __uriToNamespace__(jdata[main_entity], so_namespaces)
            sub_entities = entities[main_entity]
            x = list()
            for sub_en in sub_entities:
                ns_sub_en = __uriToNamespace__(jdata[sub_en], so_namespaces)
                x.append((jdata[sub_en], model.wv.similarity(ns_me_uri, ns_sub_en)))
        rankings = __getRankings__(x)
        predicted.extend([rankings[jdata[sub_en]] for sub_en in sub_entities])
        gold.extend(list(range(1,len(sub_entities)+1)))
    sp_val = spearmanr(gold, predicted)
    res = prev_res
    res.update({'spearmanr': sp_val})
    return res


'''
spearman relatedness
'''
import numpy as np

def __getRelatedness__(e1, e2, model, method="skip"):
    iprob=0
    if method=="skip": # skip gram
        iprob = 1.0 / (1.0 + np.exp(-np.dot(model.wv.syn1neg[e2.index].T, model.wv.syn0[e1.index])))
    if method =="cbow": #cbow
        iprob = 1.0 / (1.0 + np.exp(-np.dot(model.wv.syn0[e2.index].T, model.wv.syn1neg[e1.index])))
    return iprob


def getSpearmanrRelatedness(prev_res, params):
    model = prev_res['model']
    kore_file_location = params['kore_file_location']
    w2i_file_location = params['w2i_file_location']
    method = params['method'] if "method" in params else "skip"

    with open(w2i_file_location) as f: jdata = json.load(f)
    entities = __getKoreEntities__(kore_file_location)

    predicted = list(); gold = list()
    for main_entity in entities.keys():
        me_uri = jdata[main_entity]
        sub_entities = entities[main_entity]
        x = [(jdata[sub_en], __getRelatedness__(model.wv.vocab[me_uri], model.wv.vocab[jdata[sub_en]], model, method)) for sub_en in sub_entities]
        print(x)
        rankings = __getRankings__(x)
        print(rankings)
        predicted.extend([rankings[jdata[sub_en]] for sub_en in sub_entities])
        gold.extend(list(range(1,len(sub_entities)+1)))
    sp_val = spearmanr(gold, predicted)
    res = prev_res
    res.update({'relatedness': sp_val})
    return res


'''
corect kore combined graph
'''
import re, fileinput
from rdflib import Graph
from rdflib.term import _is_valid_uri

def __getPrefixes__(file_location):
    """Summary
    Get all prefixes of a context graph

    Args:
        file_location (str): file path to context graph

    Returns:
        LIST: list of tuples (namespace, uri)
    """
    data = str()
    with open(file_location, encoding='utf-8') as fp:
        line = fp.readline()
        while "@prefix" == line[:7]:
            data += line
            line = fp.readline()
    g = Graph()
    g.parse(data = data, format = 'n3')
    prefixes = [(x[0], str(x[1])) for x in g.namespaces()]
    return prefixes


def __groupNamespaces__(prefixes):
    """Summary
    Group namespaces who point to the same uri

    Args:
        prefixes (list): list of tuples (namespace, uri)

    Returns:
        DICT: namespace = {'ns1': [], 'ns2': ['ns21', 'ns22'], etc}
    """
    unique_uris = list(set(x[1] for x in prefixes))
    namespaces = dict()
    for uri in unique_uris:
        ns = [x[0] for x in prefixes if x[1] == uri]
        ns.sort(); ns.sort(key = lambda x: len(x))
        namespaces.update({ns[0]: ns[1:]})
    return namespaces


def __getOwlNamespace__(owl_uri, prefixes):
    """Summary
    Get namespace of owl_uri
    If there are many namespaces, get the one with minimum number

    Args:
        owl_uri (str): uri of owl
        prefixes (list): list of prefixes

    Returns:
        STR: namespace of owl uri
    """
    x = [x[0] for x in prefixes if x[1] == owl_uri]
    x.sort(); x.sort(key = lambda x: len(x))
    owl_ns = x[0] if x else None
    return owl_ns


def __optimizeNamespaces__(line, namespaces):
    """Summary
    Replace different namespaces which point to the same uri
    by an unique namespace, the one with the minimum number

    Args:
        line (str): a line in .n3 file
        namespaces (dict): grouped namespaces

    Returns:
        STR: line after optimizing namespace
    """
    for ns in namespaces:
        for x in namespaces[ns]:
            p = re.compile(' {}:'.format(x))
            line = p.sub(' {}:'.format(ns), line)
    return line


def correctContextGraph(prev_res, params):
    """
    Replace equal (=) predicats by owl:sameAs
    Correct subjects <file:///.../#xxx> by <#xxx>
    Replace multi namespaces point to the same uri by an unique namespace

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
        params (dict): keys: file_location, logger
            - file_location: file path to context graph, obligatory
            - logger: optional

    Returns:
        DICT: previous result
    """
    file_location = params['file_location']
    logger = params['logger'] if 'logger' in params else None

    prefixes = __getPrefixes__(file_location)
    namespaces = __groupNamespaces__(prefixes)
    owl_uri = 'http://www.w3.org/2002/07/owl#'
    owl_ns = __getOwlNamespace__(owl_uri, prefixes)

    p1 = re.compile('file:\/\/\/.*?\.n3')
    p2 = re.compile(' = ')
    owl_sameAs = ' {}:sameAs '.format(owl_ns) if owl_ns else ' <{}sameAs> '.format(owl_uri)

    with fileinput.FileInput(file_location, inplace=True, backup='.bak') as file:
        i = 0
        for line in file:
            i += 1
            if logger: logger.info('Processing line {}'.format(i))
            if line[:7] == '@prefix': # optimize prefixes
                m = re.search('@prefix (.+?):',line)
                if m.group(1) in namespaces:
                    print(line, end='')
                else:
                    print('',end='')
            else:
                line = p1.sub('', line) # correct subject <#XXX>
                line = p2.sub(owl_sameAs, line) # correct predicat owl sameAs
                line = __optimizeNamespaces__(line, namespaces)
                print(line, end='')
    return prev_res


def __getPredNamespaces__(file_location):
    """Summary
    Get uri_namespaces of predicates

    Args:
        file_location (str): file path of context graph (n3 file)

    Returns:
        DICT: {uri: namespace}
    """
    prefixes = __getPrefixes__(file_location)
    namespaces = __groupNamespaces__(prefixes)

    data = str()
    with open(file_location, encoding='utf-8') as fp:
        line = fp.readline()
        while "@prefix" == line[:7]:
            data += line
            line = fp.readline()
    g = Graph()
    g.parse(data = data, format = 'n3')
    pred_namespaces = dict()
    for x in g.namespaces():
        if x[0] in namespaces:
            pred_namespaces.update({str(x[1]): x[0]})
    return pred_namespaces


def __uriToNamespace__(s, uri_namespaces):
    """Summary
    Abbreviate a string by using namespace

    Args:
        s (str): string to simplify
        uri_namespaces (dict): {uri: ns}

    Returns:
        STR: simplified string
    """
    if not _is_valid_uri(s): return s
    for uri in uri_namespaces:
        ns = uri_namespaces[uri]
        try:
            p = re.compile(r'^{}'.format(uri))
            s = p.sub('{}:'.format(ns), s)
        except Exception as e:
            pass
    return s


def addNamespacesToWalks(prev_res, params):
    """Summary

    Args:
        prev_res (dict): previous result
                         obligatory key(s): walks
        params (dict): keys: file_location, so_namespaces
            - file_location: file path to context graph (n3 file)
            - so_namespaces: namespaces for subjects and objects

    Returns:
        DICT: previous result with updated walks by using namespacces
    """
    res = prev_res
    walks = prev_res['walks']
    file_location = params['file_location']
    so_namespaces = params['so_namespaces']

    pred_namespaces = __getPredNamespaces__(file_location)
    walks = [list(map(str,w)) for w in walks]
    ns_walks = list()
    for walk in walks:
        ns_walk = list()
        for i, w in enumerate(walk):
            if i % 2 == 0: # ns for subject, object
                ns_walk.append(__uriToNamespace__(w, so_namespaces))
            else: # ns for predicates
                ns_walk.append(__uriToNamespace__(w, pred_namespaces))
        ns_walks.append(ns_walk)
    res['walks'] = ns_walks
    return res


def addNamespacesToUris(prev_res, params):
    """Summary

    Args:
        prev_res (dict): previous result
                         obligatory key(s): uris
        params (dict): key: so_namespaces
            - so_namespaces: namespaces for subjects and objects

    Returns:
        DICT: previous result with updated uris by using namespaces
    """
    res = prev_res
    uris = prev_res['uris']
    so_namespaces = params['so_namespaces']

    ns_uris = [__uriToNamespace__(uri, so_namespaces) for uri in uris]
    res['uris'] = ns_uris
    return res


if __name__ == '__main__':
    import time
    from Bib import Bib

    res = dict()
    paramsGetEntities = {
        'kore_file_location': 'KORE_entity_relatedness/ITCompanies.txt'
    }
    paramsGetUris = {
        'endpoint': 'http://localhost:3030/ds/query',
        'service': 'dbpedia',
        'delay': 0.1
    }
    paramsSaveUris = {
        'filename': 'kore_db_w2i_IT_Companies.json',
        'dir': 'source'
    }
    logger = Bib.setLogger('correctContextGraph', {
        'saveToFile': False,
        'isStreaming': True
    })
    paramsCorrectContextGraph = {
        'file_location': 'dbpedia_graphs/Chuck_Norris/test.n3',
        'logger': logger
    }

    workflow = [
        # {'process': getEntities, 'params': paramsGetEntities},
        # {'process': getUris, 'params': paramsGetUris},
        # {'process': saveUris, 'params': paramsSaveUris},
        # {'process': readUris, 'params': paramsSaveUris},
        {'process': correctContextGraph, 'params': paramsCorrectContextGraph},
    ]
    for i, step in enumerate(workflow):
        start = time.time()
        print('Processing {i}/{l}: {n}'.format(
            i = i+1, l = len(workflow),
            n = step["process"].__name__
            ))
        res = step['process'](res, step['params'] if 'params' in step else None)
        end = time.time()
        print('- time processing = {} seconds'.format(end-start))
        # print('- res keys = {}'.format(res.keys()))