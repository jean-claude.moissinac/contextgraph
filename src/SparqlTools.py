# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2018-11-02 01:05:52
# @Last Modified by:   vu
# @Last Modified time: 2019-03-12 15:59:49

from SPARQLWrapper import SPARQLWrapper, JSON, POST, POSTDIRECTLY, TURTLE

msg = {
	'SELECT_FAIL': 'Problem in SPARQL SELECT',
	'ASK_FAIL': 'Problem in SPARQL ASK',
	'UPDATE_FAIL': 'Problem in SPARQL UPDATE',
}

class SparqlTools:
	@staticmethod
	def select(endpoint, query, returnFormat = 'json'):
		sparql = SPARQLWrapper(endpoint)
		sparql.setQuery(query)
		sparql.setReturnFormat(returnFormat)
		query_max_length = 1800
		if len(query) > query_max_length:
			sparql.method = 'POST'

		try:
			return sparql.query().convert()
		except Exception as e:
			# print(msg['SELECT_FAIL'])
			# print(query)
			raise e


	@staticmethod
	def ask(endpoint, query):
		sparql = SPARQLWrapper(endpoint)
		sparql.setQuery(query)
		sparql.setReturnFormat(JSON)

		try:
			res = sparql.query().convert()
			return res['boolean'] if res else False
		except Exception as e:
			# print(msg['ASK_FAIL'])
			# print(query)
			raise e


	@staticmethod
	def update(updateEndpoint, query):
		sparql = SPARQLWrapper(updateEndpoint)
		sparql.setQuery(query)
		sparql.setRequestMethod(POSTDIRECTLY)
		sparql.setMethod(POST)

		try:
			sparql.query()
		except Exception as e:
			# print(msg['UPDATE_FAIL'])
			# print(query)
			raise e