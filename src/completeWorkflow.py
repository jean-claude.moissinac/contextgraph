# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2019-03-05 16:00:39
# @Last Modified by:   vu
# @Last Modified time: 2019-03-28 12:23:04


'''
0. set loggers
'''
import datetime
import sys
from Bib import Bib
import kore_Uris as ku

paramsPerfLogs = {
    'log_dir': 'log',
    'log_file': 'workflow_perf_{}.log'.format(str(datetime.datetime.now()).replace(":","_")),
    'saveToFile': True,
    'isStreaming': True,
}
paramsExecLogs = {
    'log_dir': 'log',
    'log_file': 'workflow_exec_{}.log'.format(str(datetime.datetime.now()).replace(":","_")),
    'saveToFile': True,
    'isStreaming': True,
}
perf_logger = Bib.setLogger('performance_log', paramsPerfLogs)
exec_logger = Bib.setLogger('execution_log', paramsExecLogs)


'''
1. build uri list
'''
from GetUris import GetUris
import time

def getUris(sources, params):
    """Summary
    Get list of uris from source files.
    If uri of service exists, it takes that uri.
    Otherwise, word2uri is used to find uri from service.

    Args:
        sources (LIST): list of dict (keys: file_location, stype)
        params (DICT): keys: querypoint, service
            - querypoint (str): local SPARQL endpoint
            - service (str): one of three values: dbpedia, frdbpedia, wikidata

    Returns:
        LIST: list of uri
    """
    uris = list()
    for source in sources:
        uris = uris + GetUris.get(
                file_location = source['file_location'], # obligatory
                dtype = source['stype'], # obligatory
                service =  params["service"] if "service" in params else "frdbpedia", # optional, default value = 'frdbpedia'
                endpoint = params["querypoint"] if "querypoint" in params else  "http://localhost:3030/datamusee/query",
                delay = params['delay'] if 'delay' in params else 0.1,
                logger = exec_logger
            )
    uris.sort()
    return uris


def buildUriList(prev_res, params):
    """Summary
    Build uri list from previous result

    Args:
        prev_res (DICT): previous result
                         obligatory key(s): not required
        params (DICT): keys: sources, querypoint, service
            - sources: list of dict (keys: file_location, stype)
            - querypoint (str): local SPARQL endpoint
            - service (str): one of three values: dbpedia, frdbpedia, wikidata

    Returns:
        DICT: keys: uris
    """
    start = time.time()
    sources = params["sources"]
    uris = getUris(sources, params) # etape 1
    res = {"uris": uris}
    end = time.time()
    perf_logger.info('Time to get uris = {t} seconds'.format(t = end - start))
    return res


'''
2. build context
'''
from BuildContext import BuildContext
import os, time
from Bib import Bib

def getSavedGraphs(gdir):
    """Summary
    Get list of context graph file in a directory

    Args:
        gdir (str): directory where looking for N3 files

    Returns:
        LIST: list of N3 files in given directory
    """
    if not os.path.isdir(gdir): os.mkdir(gdir)
    gfiles = [f for f in os.listdir(gdir) if f.endswith('n3')]
    return gfiles


def buildContext(uri, params, builder = None):
    """Summary
    Generate context graph from a uri then save it in n3 file.

    Args:
        uri (str): uri to get context graph
        params (dict): keys: service, depth, graph_dir
            - service: SPARQL endpoint of dbpedia, frdbpedia or wikidata.
                       value by default: http://fr.dbpedia.org/sparql
            - depth: depth to get context graph.
                     value by default: 2
            - graph_dir: directory to contain file of context graph
                         value by default: graphs
        builder (BuildContext, optional): builder to build context graph

    Returns:
        BuildContext: builder to build context graph
    """
    exec_logger.info('[buildContext] uri = {}'.format(uri))

    service = params['service'] if 'service' in params else 'http://fr.dbpedia.org/sparql'
    depth = params['depth'] if 'depth' in params else 2
    gdir = params['graph_dir'] if 'graph_dir' in params else 'graphs'

    savedGraphs = getSavedGraphs(gdir)
    fname = 'kga{depth}_{gname}.n3'.format(
                depth = depth,
                gname = Bib.removeAccents(uri.split('/')[-1]).replace(":", "_")
            )
    if fname in savedGraphs: return builder

    start = time.time()
    if not builder: builder = BuildContext(service, depth)
    g = builder.build(uri, params)
    file_location = os.path.join(gdir, fname)
    g.serialize(file_location, format='n3', encoding='utf-8')
    end = time.time()

    exec_logger.info('[buildContext] Context graph is saved at {}'.format(file_location))
    exec_logger.info('[buildContext] Duration = {} seconds'.format(end - start))
    return builder


def contextBuilder(prev_res, params):
    """Summary
    Build context graphs from previous result.
    Each context graph is saved to a seperate n3 file.

    Args:
        prev_res (dict): previous result.
                         obligatory key(s): uris (list of uri, seeds for the context)
        params (dict): keys: blacklist, service
            - blacklist (list): list of predicates and objects to avoid adding to context graph
            - service (str): SPAQL endpoint from which we build the context graph

    Returns:
        DICT: keys: uris (list of uri)
    """
    uris = prev_res["uris"]
    start = time.time()
    builder = None
    for uri in uris:
        builder = buildContext(uri, params, builder = builder)
    end = time.time()
    perf_logger.info('Time to build context for all {n} uris = {t} seconds'.format(n = len(uris), t = end - start))
    return prev_res


'''
3. load context
'''
import os, time
from rdflib import Graph

def loadContextGraph(file_location):
    """Summary
    Load context graph from file

    Args:
        file_location (str): file path of context graph file

    Returns:
        Graph: loaded graph
    """
    g = Graph()
    start = time.time()
    g.parse(file_location, format='n3')
    end = time.time()
    perf_logger.info('time to load graph = {} seconds'.format(end - start))
    return g


def loadMultiGraphs(contextdir, aggregated_file, show_progress=True):
    """Summary
    Load context graph(s) from a directory.

    Args:
        contextdir (str): directory of context graphs
        aggregated_file (str): relative file name of complete graph
            if this file doesn't exist in context directory, the method will load all
                context graphs in directory then create this file which contains complete graph.
            if it exists, only this file is loaded.
        show_progress (bool, optional): display progress of load context graphs
            value by default: True

    Returns:
        Graph: the complete graph loaded from n3 files in context directory
    """
    savedGraphs = getSavedGraphs(contextdir)
    l = len(savedGraphs)

    start = time.time()
    aggregated_file_location = os.path.join(contextdir, aggregated_file)

    if os.path.isfile(aggregated_file_location):
        g = loadContextGraph(aggregated_file_location)

        end = time.time()
        exec_logger.info('[loadMultiGraphs] number of graphs = 1')
        perf_logger.info('time to load = {} seconds'.format(end - start))
        if show_progress:
            print('number of graphs = 1')
            print('time to load = {} seconds'.format(end - start))
    else:
        g = Graph()

        for i, f in enumerate(savedGraphs):
            if show_progress:
                exec_logger.info('[loadMultiGraphs] Loading graph {i}/{l}: {f}'.format(f=f, i=i + 1, l=l))
            s = time.time()
            fname = os.path.join(contextdir, f)
            try:
                g.parse(fname, format='n3')
            except:
                exec_logger.info("[loadMultiGraphs] Error loading and parsing {}:{}".format(fname, sys.exc_info()[0]))
            e = time.time()
            if show_progress:
                print('-- time to load graph {i} = {d} seconds'.format(i=i + 1, d=(e - s)))
                print('-- time to load {n} graphs = {d} seconds'.format(n=i + 1, d=(e - start)))
            perf_logger.info('-- time to load graph {i} = {d} seconds'.format(i=i + 1, d=(e - s)))
            perf_logger.info('-- time to load {n} graphs = {d} seconds'.format(n=i + 1, d=(e - start)))

        if not os.path.isdir(contextdir): os.mkdir(contextdir)
        g.serialize(aggregated_file_location, format='n3')

        end = time.time()
        exec_logger.info('[loadMultiGraphs] number of graphs = {}'.format(l))
        perf_logger.info('time to load = {} seconds'.format(end - start))
        if show_progress:
            print('number of graphs = {}'.format(l))
            print('time to load = {} seconds'.format(end - start))
    return g


def loadContext(prev_res, params):
    """Summary
    Load context graph(s) from previous result.

    Args:
        prev_res (dict): previous result.
                         obligatory key(s): not required.
        params (dict): keys: filename, contextdir
            - filename: filename which/to contain(s) complete graph.
                if this file does not exist in the directory, all graphs in contextdir
                directory will be loaded then saved to this file.
                otherwise, only context graph in this file is loaded.
            - contextdir: directory which contains context graph(s).

    Returns:
        DICT: previous result with updated key 'graph'
    """
    res = prev_res
    if "filename" in params:
        filename = params["filename"]
        contextdir = params["contextdir"]
        start = time.time()
        if prev_res and "graph" in prev_res:
            g = prev_res["graph"]
        else:
            g = loadMultiGraphs(contextdir, filename)
        end = time.time()
        perf_logger.info('Time to load context = {} seconds'.format(end - start))
        res.update({"graph": g})
    else:
        exec_logger.error("[loadContext] loadContext must have a parameter filename")
        results = {"error": "loadContext must have a parameter filename"}
    return res


'''
4. append graph
'''
from rdflib import Graph

def appendGraph(prev_res, params):
    """Summary
    Append a graph into previous result.
    If 'graph' key exists in previous result, the append graph will be added to it.
    Otherwise, 'graph' key will be created and contains append graph.

    Args:
        prev_res (dict): previous result.
                         obligatory key(s): not required
        params (dict): key: file_location, format
            - file_location: file path to context graph, obligatory
            - format: format of context graph file, optional
                      value by default, n3

    Returns:
        DICT: previous result with appended graph into its 'graph'
    """
    g = prev_res["graph"] if "graph" in prev_res else Graph()

    # if "file_location" in params:
    #     g.parse(params["file_location"], format='n3')
    #     res["graph"] = g
    fm = params['format'] if 'format' in params else 'n3'
    g.parse(params['file_location'], format=fm)
    res = prev_res
    res.update({'graph': g})
    return res


'''
5. load graph
'''
def loadGraph(prev_res, params):
    """Summary
    Load graph from a file to previous result.
    If 'graph' key exists in previous result, it will be overwrited.

    Args:
        prev_res (dict): previous result.
                         obligatory key(s): not required
        params (dict): key: file_location, format
            - file_location: file path to context graph, obligatory
            - format: format of context graph file, optional
                      value by default, n3

    Returns:
        DICT: previous result with appended graph into its 'graph'
    """
    g = Graph()
    g.parse(params['file_location'], format='n3')
    res = prev_res
    res.update({'graph': g})
    # if "file_location" in params:
    #     res = appendGraph(prev_res, params)
    return res


'''
6. save graph
'''
def saveGraph(prev_res, params):
    """Summary
    Save graph in previous result (if it exists) to a n3 file

    Args:
        prev_res (dict): previous result
                         obligatory key(s): graph
        params (dict): key: file_location

    Returns:
        DICT: previous result
    """
    if "graph" in prev_res:
        g = prev_res["graph"]
        g.serialize(params["file_location"], format='n3')
    return prev_res


'''
7. generate graph walks
'''
from Walks import Walks

def generateWalks(prev_res, params, functionmode):
    """Summary
    Genereate walks following a function mode

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
                         optional key(s): graph
        params (dict): keys: uris, depth, nb_walks, only_uri, filename, contextdir
            - uris: list of uris, optional.
                    if it exists prev_res['uris'], it takes list of uris from prev_res
                    otherwise, it takes the list of uris from params.
            - depth: length of walks - 1
            - nb_walks: number of walks to get
            - only_uri: True or False. By default, False.
                    False to get all literal and uri neighbors
                    True to get only uri neighbors.
            - filename, contextdir: to get graph if it doesn't exist in prev_res, optional
        functionmode (str): one of these mode: Walks.getAllWalksDepth2,
            Walks.getRandomWalks, Walks.getRandomWalksPlusD2, Walks.getBfsWalks,
            Walks.getBfsRandomWalks, Walks.getBfsRandomWalksPlusD2, Walks.getTfIdfWalks

    Returns:
        DICT: previous result with updated keys: graph, uris (if it hasn't) and walks
    """
    res = prev_res
    uris = params["uris"] if 'uris' in params else prev_res['uris']
    depth = params["depth"]
    nb_walks = params["nb_walks"]
    only_uri = params["only_uri"] if 'only_uri' in params else False

    start = time.time()
    if "graph" in prev_res:
        g = prev_res["graph"]
    else:
        if "filename" in params and "contextdir" in params:
            filename = params["filename"]
            contextdir = params["contextdir"]
            g = loadMultiGraphs(contextdir, filename)
            res.update({'graph': g})

    walks_uriref = list()
    for uri in uris:
        s = time.time()
        walks_uriref += functionmode(g, uri, nb_walks, depth, None, None, only_uri)  # random walks
        e = time.time()
        exec_logger.info('[generateWalks] Time to get walks of uri {u} = {t} seconds'.format(u=uri, t=e - s))
    end = time.time()
    perf_logger.info('Time to get walks of all {n} uris = {t} seconds'.format(n=len(uris), t=end - start))

    walks = list()
    for w in walks_uriref: walks.append(list(map(str, w)))

    res.update({"uris": uris, "walks": walks})
    return res


def generateRandomWalks(prev_res, params):
    """Summary
    Generate random walks from previous result

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
                         optional key(s): graph
        params (dict): keys: uris, depth, nb_walks, only_uri, filename, contextdir
            - uris: list of uris, optional.
                    if it exists prev_res['uris'], it takes list of uris from prev_res
                    otherwise, it takes the list of uris from params.
            - depth: length of walks - 1
            - nb_walks: number of walks to get
            - only_uri: True or False. By default, False.
                    False to get all literal and uri neighbors
                    True to get only uri neighbors.
            - filename, contextdir: to get graph if it doesn't exist in prev_res, optional

    Returns:
        DICT: previous result with updated keys: graph, uris (if it hasn't) and walks
    """
    wgen = Walks()
    return generateWalks(prev_res, params, wgen.getRandomWalks)



def generateRandomWalksPlusD2(prev_res, params):
    """Summary
    Generate random walks plus walks of depth 2 from previous result

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
                         optional key(s): graph
        params (dict): keys: uris, depth, nb_walks, only_uri, filename, contextdir
            - uris: list of uris, optional.
                    if it exists prev_res['uris'], it takes list of uris from prev_res
                    otherwise, it takes the list of uris from params.
            - depth: length of walks - 1
            - nb_walks: number of walks to get
            - only_uri: True or False. By default, False.
                    False to get all literal and uri neighbors
                    True to get only uri neighbors.
            - filename, contextdir: to get graph if it doesn't exist in prev_res, optional

    Returns:
        DICT: previous result with updated keys: graph, uris (if it hasn't) and walks
    """
    wgen = Walks()
    return generateWalks(prev_res, params, wgen.getRandomWalksPlusD2)

def generateBfsRandomWalks(prev_res, params):
    """Summary
    Generate BFS random walks from previous result

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
                         optional key(s): graph
        params (dict): keys: uris, depth, nb_walks, only_uri, filename, contextdir
            - uris: list of uris, optional.
                    if it exists prev_res['uris'], it takes list of uris from prev_res
                    otherwise, it takes the list of uris from params.
            - depth: length of walks - 1
            - nb_walks: number of walks to get
            - only_uri: True or False. By default, False.
                    False to get all literal and uri neighbors
                    True to get only uri neighbors.
            - filename, contextdir: to get graph if it doesn't exist in prev_res, optional

    Returns:
        DICT: previous result with updated keys: graph, uris (if it hasn't) and walks
    """
    wgen = Walks()
    return generateWalks(prev_res, params, wgen.getBfsRandomWalks)

def generateBfsRandomWalksPlusD2(prev_res, params):
    """Summary
    Generate BFS random walks plus depth 2 from previous result

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
                         optional key(s): graph
        params (dict): keys: uris, depth, nb_walks, only_uri, filename, contextdir
            - uris: list of uris, optional.
                    if it exists prev_res['uris'], it takes list of uris from pres_res
                    otherwise, it takes the list of uris from params.
            - depth: length of walks - 1
            - nb_walks: number of walks to get
            - only_uri: True or False. By default, False.
                    False to get all literal and uri neighbors
                    True to get only uri neighbors.
            - filename, contextdir: to get graph if it doesn't exist in prev_res, optional

    Returns:
        DICT: previous result with updated keys: graph, uris (if it hasn't) and walks
    """
    wgen = Walks()
    return generateWalks(prev_res, params, wgen.getBfsRandomWalksPlusD2)


def generateWalksD2(prev_res, params):
    """Summary
    Get all walks with depth 2

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
                         optioanl key(s): uri
        params (dict): keys: uris, only_uri (optional)

    Returns:
        DICT: previous result with updated key 'walks'
    """
    wgen = Walks()
    res = prev_res
    uris = params["uris"] if 'uris' in params else prev_res['uris']
    only_uri = params["only_uri"] if 'only_uri' in params else False

    start = time.time()
    if "graph" in prev_res:
        g = prev_res["graph"]
    else:
        if "filename" in params and "contextdir" in params:
            filename = params["filename"]
            contextdir = params["contextdir"]
            g = loadMultiGraphs(contextdir, filename)
            res.update({'graph': g})

    walks_uriref = list()
    for uri in uris:
        s = time.time()
        walks_uriref += wgen.getAllWalksDepth2(g, uri, only_uri)
        e = time.time()
        exec_logger.info('[generateWalks] Time to get walks of uri {u} = {t} seconds'.format(u=uri, t=e - s))
    end = time.time()
    perf_logger.info('Time to get walks of all {n} uris = {t} seconds'.format(n=len(uris), t=end - start))

    walks = list()
    for w in walks_uriref: walks.append(list(map(str, w)))

    res.update({"uris": uris, "walks": walks})
    return res


def generateTfIdfWalks(prev_res, params):
    wgen = Walks()
    return generateWalks(prev_res, params, wgen.getTfIdfWalks)

def generateBlacklistWalks(prev_res, params):
    wgen = Walks()
    return generateWalks(prev_res, params, wgen.getBlacklistWalks)


'''
8. save/load/serialize walks
'''
import pickle, os, json

def saveWalks(prev_res, params):
    """Summary
    Save walks from previous result to a text file .walks

    Args:
        prev_res (dict): previous result
                         obligatory key(s): walks
        params (dict): keys: walksdir, graph_name, typealgo, depth, nb_walks
            - walksdir: directory to contain walks file.
            - graph_name, typealgo, depth, nb_walks: to create file location

    Returns:
        DICT: previous result
    """
    walksdir = params["walksdir"] if "walksdir" in params else ""
    graph_name = params["graph_name"] if "graph_name" in params else ""
    typealgo = params["typealgo"] if "typealgo" in params else ""
    max_path_depth = params["depth"] if "depth" in params else 0
    walks_per_graph = params["nb_walks"] if "nb_walks" in params else 0

    fname = 'model_{graph}_{algo}_d{depth}_{walks}.walks'.format(
        graph = graph_name,
        algo = typealgo,
        depth = max_path_depth,
        walks = walks_per_graph
        )
    file_location = os.path.join(walksdir, fname)
    if walksdir and not os.path.isdir(walksdir): os.mkdir(walksdir)

    with open(file_location, "w", encoding="utf-8") as wfile:
        wfile.write(json.dumps(prev_res["walks"]))
    return prev_res

def loadWalks(prev_res, params):
    """Summary
    Load walks from a text file .walks to previous result

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
        params (dict): keys: walksdir, graph_name, typealgo, depth, nb_walks,
                             file_location
            - walksdir: directory which contains walks file.
            - graph_name, typealgo, depth, nb_walks: to create file location to load
            - file_location: if it exists, it will be used directly,
                             no need create filename to load

    Returns:
        DICT: previous result with updated key 'walks'
    """
    if 'file_location' not in params:
        walksdir = params["walksdir"] if "walksdir" in params else ""
        graph_name = params["graph_name"] if "graph_name" in params else ""
        typealgo = params["typealgo"] if "typealgo" in params else ""
        max_path_depth = params["depth"] if "depth" in params else 0
        walks_per_graph = params["nb_walks"] if "nb_walks" in params else 0

        fname = 'model_{graph}_{algo}_d{depth}_{walks}.walks'.format(
            graph = graph_name,
            algo = typealgo,
            depth = max_path_depth,
            walks = walks_per_graph
            )
        file_location = os.path.join(walksdir, fname)
    else:
        file_location = params['file_location']

    res = prev_res
    with open(file_location, encoding="utf-8") as wfile:
        res["walks"] = json.load(wfile)
    return res


def saveWalksB(prev_res, params):
    """Summary
    Save walks from previous result to a binary file .walks

    Args:
        prev_res (dict): previous result
                         obligatory key(s): walks
        params (dict): keys: walksdir, graph_name, typealgo, depth, nb_walks
            - walksdir: directory to contain walks file.
            - graph_name, typealgo, depth, nb_walks: to create file location

    Returns:
        DICT: previous result
    """
    walksdir = params["walksdir"] if "walksdir" in params else ""
    graph_name = params["graph_name"] if "graph_name" in params else ""
    typealgo = params["typealgo"] if "typealgo" in params else ""
    max_path_depth = params["depth"] if "depth" in params else 0
    walks_per_graph = params["nb_walks"] if "nb_walks" in params else 0

    fname = 'model_{graph}_{algo}_d{depth}_{walks}b.walks'.format(
        graph = graph_name,
        algo = typealgo,
        depth = max_path_depth,
        walks = walks_per_graph
        )
    file_location = os.path.join(walksdir, fname)
    if walksdir and not os.path.isdir(walksdir): os.mkdir(walksdir)

    with open(file_location, "wb") as fp:
        pickle.dump(prev_res["walks"], fp)
    return prev_res


def loadWalksB(prev_res, params):
    """Summary
    Load walks from a binary file .walks to previous result

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
        params (dict): keys: walksdir, graph_name, typealgo, depth, nb_walks,
                            file_location
            - walksdir: directory which contains walks file.
            - graph_name, typealgo, depth, nb_walks: to create file location to load
            - file_location: if it exists, it will be used directly,
                             no need create filename to load
    Returns:
        DICT: previous result with updated key 'walks'
    """
    if 'file_location' not in params:
        walksdir = params["walksdir"] if "walksdir" in params else ""
        graph_name = params["graph_name"] if "graph_name" in params else ""
        typealgo = params["typealgo"] if "typealgo" in params else ""
        max_path_depth = params["depth"] if "depth" in params else 0
        walks_per_graph = params["nb_walks"] if "nb_walks" in params else 0

        fname = 'model_{graph}_{algo}_d{depth}_{walks}b.walks'.format(
            graph = graph_name,
            algo = typealgo,
            depth = max_path_depth,
            walks = walks_per_graph
            )
        file_location = os.path.join(walksdir, fname)
    else:
        file_location = params['file_location']

    res = prev_res
    with open(file_location, "rb") as fp:
        res["walks"] = pickle.load(fp)
    return res


def serializeWalks(prev_res, params):
    """Summary
    Serialize walks from previous result to text file .txt

    Args:
        prev_res (dict): previous result
                         obligatory key(s): walks
        params (dict): keys: walksdir, graph_name, typealgo, depth, nb_walks,
                            file_location
            - walksdir: directory which contains walks file.
            - graph_name, typealgo, depth, nb_walks: to create file location to load
            - file_location: if it exists, it will be used directly,
                             no need create filename to load

    Returns:
        DICT: previous result
    """
    if 'file_location' not in params:
        walksdir = params["walksdir"] if "walksdir" in params else ""
        graph_name = params["graph_name"] if "graph_name" in params else ""
        typealgo = params["typealgo"] if "typealgo" in params else ""
        max_path_depth = params["depth"] if "depth" in params else 0
        walks_per_graph = params["nb_walks"] if "nb_walks" in params else 0

        fname = 'model_{graph}_{algo}_d{depth}_{walks}.txt'.format(
            graph = graph_name,
            algo = typealgo,
            depth = max_path_depth,
            walks = walks_per_graph
            )
        file_location = os.path.join(walksdir, fname)
        if os.path.isfile(file_location): os.remove(file_location)
    else:
        file_location = params['file_location']

    walks = prev_res["walks"]
    # for walk in walks:
    #     walk_serialized = ' --> '.join(walk)
    #     print(walk_serialized, file=open(file_location, "a", encoding="utf-8"))
    with open(file_location, "w", encoding="utf-8") as wfile:
        for walk in walks:
            serialized_walk = ' --> '.join(walk)
            wfile.write(serialized_walk)
    return prev_res


'''
9. buil model
'''
from gensim.models.word2vec import Word2Vec

def modelFromWalks(walks, params=None):
    """Summary
    Build model from walks

    Args:
        walks (list): list of walks
        params (dict, optional): keys: size, window, worker, sg, hs, iter, negative, min_count
            - size (int): dimension of feature vector, value by default: 500
            - window (int): maximum distance between the current and predicted word
                within a sentence, value by default: 5
            - worker (int): number of thread using to train model, value by default: 4
            - sg (int): 0 - CBOW, 1 - skip-gram
            - hs (int): 1 - hierarchical softmax, 0 + negative > 0: negative sampling
                value by default: 0
            - iter (int): number of iterations over the corpus, value by default: 5
            - negative (int): number of noise words should be drawn, value by default: 15
            - min_count (int): ignore all words with total frequency lower than this value
                value by default: 1

    Returns:
        Word2Vec: a model from walks
    """
    size = params['size'] if params and 'size' in params else 500
    window = params['window'] if params and 'window' in params else 5
    workers = params['workers'] if params and 'workers' in params else 4
    sg = params['sg'] if params and 'sg' in params else 1
    hs = params['hs'] if params and 'hs' in params else 0
    num_iter = params['iter'] if params and 'iter' in params else 10
    negative = params['negative'] if params and 'negative' in params else 15
    min_count = params['min_count'] if params and 'min_count' in params else 1

    sentences = [list(map(str, x)) for x in walks]
    model = Word2Vec(sentences,
                # JCM 20240707 size = size,
                window = window,
                workers = workers, sg = sg, hs = hs,
                epochs = num_iter, # iter = num_iter,
                negative = negative,
                min_count = min_count)
    model.train(sentences, total_examples=len(sentences), epochs=10)
    return model


def buildModel(prev_res, params=None):
    """Summary
    Build model from previous result

    Args:
        prev_res (dict): previous result
                         obligatory key(s): walks
                         optional key(s): uris
        params (dict, optional): keys: size, window, worker, sg, hs, iter,
            negative, min_count, uris
            - size (int): dimension of feature vector, value by default: 500
            - window (int): maximum distance between the current and predicted word
                within a sentence, value by default: 5
            - worker (int): number of thread using to train model, value by default: 4
            - sg (int): 0 - CBOW, 1 - skip-gram
            - hs (int): 1 - hierarchical softmax, 0 + negative > 0: negative sampling
                value by default: 0
            - iter (int): number of iterations over the corpus, value by default: 5
            - negative (int): number of noise words should be drawn, value by default: 15
            - min_count (int): ignore all words with total frequency lower than this value
                value by default: 1

    Returns:
        DICT: previous result with updated key 'model'
    """
    walks = prev_res["walks"]
    if "uris" in prev_res:
        uris = prev_res["uris"]
    else:
        uris = params["uris"] if params and "uris" in params else None

    start = time.time()
    model = modelFromWalks(walks, params)
    end = time.time()
    perf_logger.info('Time to build model = {t} seconds'.format(t = end - start))

    res = prev_res
    if uris: res["refuri"] = uris[0]
    res["model"] = model
    return res


'''
10. save/load model
'''
def saveModel(prev_res, params):
    """Summary
    Save model from previous result to a file .model

    Args:
        prev_res (dict): previous result
                         obligatory key(s): model
        params (dict): keys: model_dir, graph_name, typealgo, depth, nb_walks
            these params is to create model file location.
            - model_dir (str): directory which contains model file
                value by default: result
            - graph_name (str): value by default: ''
            - typealgo (str): value by default: ''
            - depth (int): length of walks in model - 1, value by default: 0
            - nb_walk (int): number of walks in model, value by default: 0

    Returns:
        DICT: previous result
    """
    model = prev_res["model"]

    model_dir = params["model_dir"] if "model_dir" in params else "result"
    graph_name = params["graph_name"] if "graph_name" in params else ""
    typealgo = params["typealgo"] if "typealgo" in params else ""
    max_path_depth = params["depth"] if "depth" in params else 0
    walks_per_graph = params["nb_walks"] if "nb_walks" in params else 0

    fname = 'model_{graph}_{algo}_d{depth}_{walks}.model'.format(
            graph = graph_name,
            algo = typealgo,
            depth = max_path_depth,
            walks = walks_per_graph
        )

    if not os.path.isdir(model_dir): os.mkdir(model_dir)
    file_location = os.path.join(model_dir, fname)
    model.save(file_location)
    return prev_res


def buildModelFilepath(params):
    model_dir = params["model_dir"] if "model_dir" in params else "result"
    graph_name = params["graph_name"] if "graph_name" in params else ""
    typealgo = params["typealgo"] if "typealgo" in params else ""
    max_path_depth = params["depth"] if "depth" in params else 0
    walks_per_graph = params["nb_walks"] if "nb_walks" in params else 0

    fname = 'model_{graph}_{algo}_d{depth}_{walks}.model'.format(
            graph = graph_name,
            algo = typealgo,
            depth = max_path_depth,
            walks = walks_per_graph
        )
    file_location = os.path.join(model_dir, fname)
    return file_location


def loadModel(prev_res, params):
    """Summary
    Load model from a model file .model to previous result

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
        params (dict): keys: model_dir, graph_name, typealgo, depth, nb_walks,
                             file_location
                these 5 first params is to create model file location to load.
            - model_dir (str): directory which contains model file
                value by default: result
            - graph_name (str): value by default: ''
            - typealgo (str): value by default: ''
            - depth (int): length of walks in model - 1, value by default: 0
            - nb_walk (int): number of walks in model, value by default: 0
            - file_location: if it exists, it will be used directly,
                             no need create filename to load

    Returns:
        DICT: previous result with updated key model
    """
    if 'file_location' not in params:
        file_location = buildModelFilepath(params)
    else:
        file_location = params['file_location']
    model = Word2Vec.load(file_location)
    res = prev_res
    res["model"] = model
    return res


'''
12. CorePersons
'''
# import datamusee.smalltools.graphTools.graphTools.graphTools as gtoo
import GraphTools

def getCorePersons():
    gt = GraphTools()
    query = '''
        PREFIX schema: <http://schema.org/>
        SELECT DISTINCT ?s from <http://givingsense.eu/datamusee/onto/contextgraphwithtypes>
        WHERE {
            ?seed ?l ?s .
            ?s ?p ?o ;
            a <http://dbpedia.org/ontology/Artist>
            FILTER (?seed in (
                <http://fr.dbpedia.org/resource/Musée_Zadkine>,
                <http://fr.dbpedia.org/resource/Paris_Musées>,
                <http://fr.dbpedia.org/resource/Maison_de_Victor_Hugo>,
                <http://fr.dbpedia.org/resource/Musée_Cognacq-Jay>,
                <http://fr.dbpedia.org/resource/Palais_Galliera>,
                <http://fr.dbpedia.org/resource/Musée_du_Général-Leclerc-de-Hauteclocque-et-de-la-Libération-de-Paris_&_musée_Jean-Moulin>,
                <http://fr.dbpedia.org/resource/Petit_Palais>,
                <http://fr.dbpedia.org/resource/Musée_du_Général-Leclerc-de-Hauteclocque-et-de-la-Libération-de-Paris_–_musée_Jean-Moulin>,
                <http://fr.dbpedia.org/resource/Palais_Galliera,_musée_de_la_Mode_de_la_Ville_de_Paris>,
                <http://fr.dbpedia.org/resource/Musée_Bourdelle>,<http://fr.dbpedia.org/resource/Musée_Cernuschi>,
                <http://fr.dbpedia.org/resource/Musée_d'art_moderne_de_la_ville_de_Paris>,
                <http://fr.dbpedia.org/resource/Crypte_archéologique_du_parvis_Notre-Dame>,
                <http://fr.dbpedia.org/resource/Maison_de_Balzac>,
                <http://fr.dbpedia.org/resource/Musée_de_la_vie_romantique>,
                <http://fr.dbpedia.org/resource/Musée_Carnavalet>
            ))
        }'''
    service = 'http://fr.dbpedia.org/sparql'
    res = gt.select(query, service)
    return  [r["s"]["value"] for r in res] if res else []


'''
13. append walks
'''
import json, pickle

def appendWalks(prev_res, params):
    """Summary
    Append walks from text file .walks to previous result

    Args:
        prev_res (dict): previous resullt
                         obligatory key(s): not required
                         optional key(s): walks
        params (dict): key: file_location

    Returns:
        DICT: previous result with concatenated 'walks'
    """
    res = prev_res
    walks = prev_res['walks'] if 'walks' in prev_res else list()
    with open(params['file_location'], encoding="utf-8") as wfile:
        walks += json.load(wfile)
    res['walks'] = walks
    return res


def appendWalksB(prev_res, params):
    """Summary
    Append walks from binary file .walks to previous result

    Args:
        prev_res (dict): previous resullt
                         obligatory key(s): not required
                         optional key(s): walks
        params (dict): key: file_location

    Returns:
        DICT: previous result with concatenated 'walks'
    """
    res = prev_res

    walks = prev_res['walks'] if 'walks' in prev_res else list()
    with open(params['file_location'], "rb") as fp:
        walks += pickle.load(fp)

    res['walks'] = walks
    return res


def __appendGeneratedWalks(prev_res, params, functionmode):
    """Summary
    Append genereated walks following a function mode to previous result

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
                         optional key(s): graph
        params (dict): keys: uris, depth, nb_walks, only_uri, filename, contextdir
            - uris: list of uris, optional.
                    if it exists prev_res['uris'], it takes list of uris from prev_res
                    otherwise, it takes the list of uris from params.
            - depth: length of walks - 1
            - nb_walks: number of walks to get
            - only_uri: True or False. By default, False.
                    False to get all literal and uri neighbors
                    True to get only uri neighbors.
            - filename, contextdir: to get graph if it doesn't exist in prev_res, optional
        functionmode (str): one of these mode: Walks.getAllWalksDepth2,
            Walks.getRandomWalks, Walks.getRandomWalksPlusD2, Walks.getBfsWalks,
            Walks.getBfsRandomWalks, Walks.getBfsRandomWalksPlusD2, Walks.getTfIdfWalks

    Returns:
        DICT: previous result with updated keys: graph, uris (if it hasn't) and walks
    """
    res = prev_res
    uris = params["uris"] if 'uris' in params else prev_res['uris']
    depth = params["depth"]
    nb_walks = params["nb_walks"]
    only_uri = params["only_uri"] if 'only_uri' in params else False

    start = time.time()
    if "graph" in prev_res:
        g = prev_res["graph"]
    else:
        if "filename" in params and "contextdir" in params:
            filename = params["filename"]
            contextdir = params["contextdir"]
            g = loadMultiGraphs(contextdir, filename)
            res.update({'graph': g})

    walks_uriref = list()
    for uri in uris:
        s = time.time()
        walks_uriref += functionmode(g, uri, nb_walks, depth, None, None, only_uri)
        e = time.time()
        exec_logger.info('[generateWalks] Time to get walks of uri {u} = {t} seconds'.format(u=uri, t=e - s))
    end = time.time()
    perf_logger.info('Time to get walks of all {n} uris = {t} seconds'.format(n=len(uris), t=end - start))

    walks = prev_res['walks'] if 'walks' in prev_res else list()
    for w in walks_uriref: walks.append(list(map(str, w)))

    if 'uris' not in res:
        res.update({"uris": uris, "walks": walks})
    else:
        res.update({"walks": walks})
    return res


def appendGeneratedRandomWalks(prev_res, params):
    """Summary
    Append generated random walks to previous result

    Args:
        prev_res (dict): previous result
                         obligatory key(s): not required
                         optional key(s): graph
        params (dict): keys: uris, depth, nb_walks, only_uri, filename, contextdir
            - uris: list of uris, optional.
                    if it exists prev_res['uris'], it takes list of uris from prev_res
                    otherwise, it takes the list of uris from params.
            - depth: length of walks - 1
            - nb_walks: number of walks to get
            - only_uri: True or False. By default, False.
                    False to get all literal and uri neighbors
                    True to get only uri neighbors.
            - filename, contextdir: to get graph if it doesn't exist in prev_res, optional

    Returns:
        DICT: previous result with updated keys: graph, uris (if it hasn't) and walks
    """
    wgen = Walks()
    return __appendGeneratedWalks(prev_res, params, wgen.getRandomWalks)
'''
blacklist
'''
__blacklist = {
    'pred': [
        # '<http://www.w3.org/2000/01/rdf-schema#comment>',
        # '<http://www.w3.org/2002/07/owl#sameAs>',
        # '<http://dbpedia.org/ontology/abstract>',
        '<http://fr.dbpedia.org/property/visiteurs>',
        # the following lines concern dbpedia stewardship data
        '<http://dbpedia.org/ontology/wikiPageRevisionID>',
        '<http://dbpedia.org/ontology/wikiPageOutDegree>',
        '<http://dbpedia.org/ontology/wikiPageID>',
        '<http://dbpedia.org/ontology/wikiPageLength>',
        '<http://fr.dbpedia.org/property/wikiPageUsesTemplate>',
    ],
    'obj': [
        '<http://www.w3.org/2002/07/owl#Thing>',
    ],
    'terminalnode': [
        '<http://www.w3.org/2004/02/skos/core#Concept>',
        '<http://www.wikidata.org/entity/Q5>',
        '<http://www.w3.org/2002/07/owl#Class>',
        '<http://schema.org/Place>',
        '<http://schema.org/Person>',
        '<http://dbpedia.org/ontology/PopulatedPlace>',
        '<http://www.wikidata.org/entity/Q486972>',
        '<http://schema.org/CreativeWork>',
        '<http://schema.org/Book>',
        '<http://xmlns.com/foaf/0.1/Document>',
        '<http://xmlns.com/foaf/0.1/Person>',
        '<http://www.wikidata.org/entity/Q1028181>',
        '<http://www.wikidata.org/entity/Q1028181>',
        '<http://schema.org/AdministrativeArea>',
        '<http://www.wikidata.org/entity/Q11032>',
        '<http://www.wikidata.org/entity/Q571>',
        '<http://www.wikidata.org/entity/Q4989906>',
        '<http://www.wikidata.org/entity/Q82955>',
        '<http://www.wikidata.org/entity/Q23413>',
        '<http://www.wikidata.org/entity/Q3455524>',
        '<http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#Agent>',
        '<http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#NaturalPerson>',
        '<http://www.wikidata.org/entity/Q215627>',
        '<http://www.wikidata.org/entity/Q34442>',
        '<http://www.wikidata.org/entity/Q483501>',
        '<http://www.wikidata.org/entity/Q386724>',
        '<http://www.wikidata.org/entity/Q36180>',
        '<http://purl.org/ontology/bibo/Book>',
        '<http://www.wikidata.org/entity/Q1092563>',
        '<http://www.wikidata.org/entity/Q30185>',
        '<http://schema.org/Continent>',
        '<http://www.wikidata.org/entity/Q6256>',
        '<http://schema.org/Country>',
        '<http://www.wikidata.org/entity/Q163740>',
        '<http://schema.org/Organization>',
        '<http://schema.org/Painting>',
        '<http://www.wikidata.org/entity/Q30461>',
        '<http://www.wikidata.org/entity/Q23442>',
        '<http://www.wikidata.org/entity/Q1370598>',
        '<http://www.wikidata.org/entity/Q43229>',
        '<http://www.wikidata.org/entity/Q30185>',
        '<http://www.wikidata.org/entity/Q901>',
        '<http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#SocialPerson>',
        '<http://schema.org/Sculpture>',
        '<http://schema.org/MusicAlbum>',
        '<http://www.wikidata.org/entity/Q2188189>',
        '<http://www.wikidata.org/entity/Q1656682>',
        '<http://schema.org/MusicGroup>',
        '<http://schema.org/Movie>',
        '<http://schema.org/Event>',
        '<http://www.wikidata.org/entity/Q482994>',
        '<http://dbpedia.org/ontology/Wikidata:Q11424>',
    ],
    'regex': [
        '(<http:\/\/fr\.dbpedia\.org\/resource\/Modèle:.*>)',
    ]
}


if __name__ == '__main__':
    paramsGetUris = {
        "sources": [
            {
                'file_location': 'data/DM/parismusees01022019.json',
                'stype': 'parismusees'
            }
        ],
        "querypoint": "http://localhost:3030/datamusee/query", # need for a sparql access point with data
        "service": "frdbpedia",
        "delay": 0.1
    }


    paramsContextBuilder = {
        "depth": 1,
        "service": 'http://dbpedia.org/sparql',
        "graph_dir": 'data/graphs_0313',

        "blacklist": __blacklist,
        "delay": 0.1,
        "show_progress": False,
        "follow_classes": False,
        "get_classes": False,
        "logger": exec_logger,
        'prev_graph': None
    }
    paramsLoadContext = {
        "filename": 'kga1_NorrisContext.n3',
        "contextdir": "data/graphs_0313",
    }
    paramsAppendGraph = {
        "file_location": "data/graphs_0313/kga1_NorrisContext.n3"
    }
    paramsLoadGraph = {
        "file_location": "data/graphs_0313/kga1_NorrisContext.n3"
    }
    paramsSaveGraph = {
        "file_location": "data/graphs_0313/saved_graph.n3"
    }
    paramsGenerateWalks = {
        "filename": 'kga1_NorrisContext.n3',
        "contextdir": "data/graphs_0313",
        # "uris": ['http://dbpedia.org/resource/Chuck_Norris'], # get uris from the previous step
        "depth": 4,
        "nb_walks": 21000
    }
    paramsSaveWalks = {
        "walksdir": "data/graphs_0313",
        # "graph_name": "kga1_NorrisContext",
        "file_location": "data/graphs_0313/model_kga1_NorrisContext_randomWalk_d4_0.walks",
        "typealgo": "randomWalk",
        "depth": 4,
        "nb_walks": 21000,
    }
    paramsBuildModel = {
        #'uris': ['http://dbpedia.org/resource/Unix_security'],
        'size': 5,
        'window': 500,
        'worker': 4,
        'sg': 1,
        'iter': 10,
        'negative': 25,
        'min_count': 1
    }
    paramsSaveModel = {
        "model_dir": "data/graphs_0313",
        "graph_name": "kga1_NorrisContext",
        "typealgo": "randomWalk",
        "depth": 4,
        "nb_walks": 21000,
    }
    paramsAppendWalks = {
        "file_location": "data/graphs_0313/model_Unix_Security_randomWalk_d4_0.walks"
    }
    paramsReadUris = {
        'filename': 'kore_db_w2i_Chuck_Norris.json',
        'dir': 'data/KORE'
    }

    workflow = [
        # {"process": buildUriList,  "params": paramsGetUris},
        # {'process': ku.readUris, 'params': paramsReadUris},
        #{"process": contextBuilder, "params": paramsContextBuilder},
        # # add a step to add the types of the entities and cut the poorly connected entities; see addTypesForImportantEntities.py
        # {"process": loadContext, "params": paramsLoadContext},
        # {"process": appendGraph, "params": paramsAppendGraph },
        # {"process": loadGraph, "params": paramsLoadGraph },
        # {"process": saveGraph, "params": paramsSaveGraph },
        # # generateRandomWalks or generateTfIdfWalks or generateBlacklistWalks
        # {"process": generateRandomWalks, "params": paramsGenerateWalks},
        # {"process": saveWalks, "params": paramsSaveWalks},
        {"process": loadWalks, "params": paramsSaveWalks},
        {"process": buildModel, "params": paramsBuildModel},
        {"process": saveModel, "params": paramsSaveModel},
        # {"process": loadModel, "params": paramsSaveModel}
        # {"process": appendWalks, "params": paramsAppendWalks}
    ]

    res = {}
    for step in workflow:
        res = step['process'](res, step['params'])