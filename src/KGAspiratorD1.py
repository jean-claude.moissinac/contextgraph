# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2019-01-14 02:06:26
# @Last Modified by:   vu
# fork from KGAspirator from Jean-Claude Moissinac by vu
# @Last Modified time: 2019-03-19 15:14:36

from GraphTools import GraphTools
from rdflib import URIRef, Literal, RDF, BNode, OWL
from rdflib.term import _is_valid_uri
import time, re, urllib

class KGAspiratorD1:
    '''
    the following dictionnary contains queries
    'subj' get all triples about the parameter 'subject' with neither objects or predicates in the blacklist
    'obj' get all triples about the parameter 'object' with neither subjects or predicates in the blacklist
    'types" select all types for the parameter
    '''
    SPARQL_QUERIES = {
        'subj': '''
            SELECT ?pred ?obj
            WHERE {
                <__subj> ?pred ?obj
                FILTER (
                    ?pred NOT IN ( __predBlacklist ) &&
                    ?obj NOT IN ( __objBlacklist )
                )
            }
        ''',
        'obj': '''
            SELECT ?subj ?pred
            WHERE {
                ?subj ?pred <__obj>
                FILTER (
                    ?pred NOT IN ( __predBlacklist )
                )
            }
        ''',
        'types': '''
            SELECT ?t
            WHERE {
                <__uri> a ?t
            }
        '''
    }

    def __init__(self, blacklist=None, delay=0.1):
        self.graphTool = None
        self.delay = delay
        self.blacklist = blacklist
        if blacklist and ("terminalnode" in blacklist):
            self.blacklist["terminalnode"] = set(blacklist["terminalnode"])
        self.cached_subj = dict()
        self.cached_obj = dict()
        self.long_delay = 1200 # 20 minutes

    def getGraphTool(self):
        if self.graphTool is not None: return self.graphTool
        self.graphTool = GraphTools()
        return self.graphTool

    def setDelay(self, delay):
        self.delay = delay


    def tuneUri(self, invalid_uri):
        x = invalid_uri.split('/')[-1]
        pos = invalid_uri.find(x)
        uri = invalid_uri[:pos] + urllib.parse.quote(x)
        return uri

    def res2RdfTerm(self, res):
        val = None
        if (res['type'] == 'literal') or (res['type'] == 'typed-literal'):
            val = Literal(res['value'])
            if 'xml:lang' in res:
                val = Literal(res['value'], lang=res['xml:lang'])
            if 'datatype' in res:
                val = Literal(res['value'], datatype=res['datatype'])
        if res['type'] == 'bnode':
            val = BNode(res["value"])
        if res['type'] == 'uri':
            if _is_valid_uri(res['value']):
                val = URIRef(res['value'])
            else:
                uri = self.tuneUri(res['value'])
                return URIRef(uri) if _is_valid_uri(uri) else val
        return val


    def addTypes(self, g, uri, service='http://fr.dbpedia.org/sparql', follow_classes=False, logger=None):
        """Summary

        Args:
            g (rdflib.Graph): graph to insert types to
            uri (str): uri of entity which we want to looking its types
            service (str, optional): SPARQL endpoint to query
            follow_classes (bool, optional): Description
            logger (None, optional): Description

        Returns:
            rdflib.Graph: completed graph

        Raises:
            e: Description
        """
        if not _is_valid_uri(uri):
            if logger: logger.info('[KGAspiratorD1] addTypes invalid uri: {}'.format(uri))
            return g

        gt = self.getGraphTool()
        query = self.SPARQL_QUERIES['types'].replace("__uri", uri)

        try:
            result = gt.select(query, service)
        except Exception as e:
            if logger:
                logger.info(e)
                logger.info('Retry uri: {}'.format(uri))
            time.sleep(self.long_delay)
            result = gt.select(query, service)
            # raise e

        if result:
            subj = URIRef(uri)
            for res in result:
                pred = RDF.type
                obj = self.res2RdfTerm(res['t'])
                if not obj: continue
                g.add((subj, pred, obj))
                if (not follow_classes) and self.blacklist:
                    self.blacklist["terminalnode"].add(URIRef(obj).n3())
        else:
            if logger: logger.info('[KGAspiratorD1] uri sans type: {}'.format(uri))
        return g


    def getExcerpt(self, g, uri, service='http://fr.dbpedia.org/sparql', follow_classes= False, logger=None):
        """Summary

        Args:
            g (TYPE): Description
            uri (TYPE): Description
            service (str, optional): Description
            follow_classes (bool, optional): Description
            logger (None, optional): Description

        Returns:
            TYPE: Description

        Raises:
            e: Description
        """
        if not _is_valid_uri(uri):
            if logger: logger.info('[KGAspiratorD1] getExcerpt invalid uri: {}'.format(uri))
            return g

        gt = self.getGraphTool()
        rdf_uri = URIRef(uri).n3()
        triple = "<__uri__> a <http://www.w3.org/2002/07/owl#Class>".replace("__uri__", uri)
        try:
            if (not follow_classes) and gt.askForTriple(triple, service): return g
        except Exception as e:
            if logger:
                logger.info(e)
                logger.info('Re ask uri: {}'.format(uri))
            time.sleep(self.long_delay)
            if (not follow_classes) and gt.askForTriple(triple, service): return g
        is_terminal = rdf_uri in self.blacklist["terminalnode"]
        if not is_terminal:
            for tststr in self.blacklist['regex']:
                found = re.search(tststr, rdf_uri)
                if found:
                    is_terminal = True
                    break
        if not is_terminal:
            if uri not in self.cached_subj:
                query = self.SPARQL_QUERIES['subj']
                query = query.replace('__predBlacklist', ','.join(self.blacklist['pred']))
                query = query.replace('__objBlacklist', ','.join(self.blacklist['obj']))
                query = query.replace('__subj', uri)

                time.sleep(self.delay)
                if logger: logger.info('[KGAspiratorD1] on uri as subject {}'.format(uri))

                try:
                    result = gt.select(query, service)
                except Exception as e:
                    if logger:
                        logger.info(e)
                        logger.info('Retry uri: {}'.format(uri))
                    time.sleep(self.long_delay)
                    result = gt.select(query, service)
                    # raise e
                self.cached_subj[uri] = result
            else:
                result = self.cached_subj[uri]

            if result:
                subj = URIRef(uri)
                for res in result:
                    pred = self.res2RdfTerm(res['pred'])
                    obj = self.res2RdfTerm(res['obj'])
                    if pred and obj:
                        g.add((subj, pred, obj))
                    if obj and (not follow_classes) and ((pred==RDF.type) or (pred==OWL.equivalentClass)):
                        self.blacklist["terminalnode"].add(URIRef(obj).n3())

            if uri not in self.blacklist['terminalnode']:
                if uri not in self.cached_obj:
                    query = self.SPARQL_QUERIES['obj']
                    query = query.replace('__service', service)
                    query = query.replace('__predBlacklist', ','.join(self.blacklist['pred']))
                    query = query.replace('__obj', uri)

                    time.sleep(self.delay)
                    if logger: logger.info('[KGAspiratorD1] on uri as object {}'.format(uri))

                    try:
                        result = gt.select(query, service)
                    except Exception as e:
                        if logger:
                            logger.info(e)
                            logger.info('Retry uri: {}'.format(uri))
                        time.sleep(self.long_delay)
                        result = gt.select(query, service)
                        # raise e
                    self.cached_obj[uri] = result
                else:
                    result = self.cached_obj[uri]

                if result:
                    obj = URIRef(uri)
                    for res in result:
                        subj = self.res2RdfTerm(res['subj'])
                        pred = self.res2RdfTerm(res['pred'])
                        g.add((subj, pred, obj))
                        if (not follow_classes) and ((pred==RDF.type) or (pred==OWL.equivalentClass)):
                            self.blacklist["terminalnode"].add(URIRef(obj).n3())

        return g


if __name__ == '__main__':
    from rdflib import Graph

    service = 'http://dbpedia.org/sparql'
    uri = "http://dbpedia.org/resource/Movie"
    blacklist = {
        'pred': [
            '<http://www.w3.org/2000/01/rdf-schema#comment>',
            '<http://www.w3.org/2002/07/owl#sameAs>',
            '<http://dbpedia.org/ontology/abstract>',
            '<http://fr.dbpedia.org/property/visiteurs>',
            # added more by Tuan
            '<http://dbpedia.org/ontology/wikiPageWikiLink>',
            '<http://purl.org/voc/vrank#hasRank>'
        ],
        'obj': [
            '<http://www.w3.org/2002/07/owl#Thing>',
            '<http://fr.dbpedia.org/resource/Modèle:Autres_projets>',
            '<http://fr.dbpedia.org/resource/Modèle:Citation>',
            '<http://fr.dbpedia.org/resource/Modèle:Date>',
            '<http://fr.dbpedia.org/resource/Modèle:ISBN>',
            '<http://fr.dbpedia.org/resource/Modèle:Message_galerie>',
            '<http://fr.dbpedia.org/resource/Modèle:P.>',
            '<http://fr.dbpedia.org/resource/Modèle:Palette>',
            '<http://fr.dbpedia.org/resource/Modèle:Portail>',
            '<http://fr.dbpedia.org/resource/Modèle:Références>',
            '<http://fr.dbpedia.org/resource/Modèle:S->',
            '<http://fr.dbpedia.org/resource/Modèle:Unité>',
            '<http://fr.dbpedia.org/resource/Modèle:Voir_homonymes>',
            '<http://dbpedia.org/ontology/ArchitecturalStructure>',
            '<http://dbpedia.org/ontology/Building>',
            '<http://www.w3.org/2003/01/geo/wgs84_pos#SpatialThing>',
            '<http://fr.dbpedia.org/resource/Modèle:...>'
        ],
        'terminalnode': [
        ],
        'regex': [
            '(<http:\/\/fr\.dbpedia\.org\/resource\/Modèle:.*>)',
        ]
    }

    kga = KGAspiratorD1(blacklist, 0.1)
    g = Graph()
    g = kga.getExcerpt(g, uri, service)
    print('number of triples = {}'.format(g.__len__()))
