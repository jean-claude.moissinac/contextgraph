This folder contains files from or derived from the KORE dataset (see https://paperswithcode.com/paper/kore-50-dywc-an-evaluation-data-set-for).

There are five categories: celebrities, IT companies, TV series, video games and Chuck Norris.

The files Celebrities.txt, ITCompanies.txt, TVSeries.txt, VideoGames.txt and Chuck_Norris.txt contain the reference data from KORE, with several entities named as the main entities in the category (e.g. for ITCompanies, Apple Inc.), each followed by a list of entities related to the main entity, arranged in descending order of relationship.

For each category, there's a kore_db_w2i_xxx.json file -where xxx is replaced by the category name- which provides an association between a named entity and a corresponding URI in DBPedia. These associations were obtained by human verification, assisted by tools such as DBPedia Spotlight.
