Angelina Jolie
	Jon Voight
	Brad Pitt
	Billy Bob Thornton
	Lara Croft
	Mr. & Mrs. Smith (2005 film)
	Jenny Shimizu
	Hackers (film)
	Chip Taylor
	Academy Award
	Cornelia Wallace
	Swakopmund
	Palisades, New York
	Battambang
	World Economic Forum
	Lenny Kravitz
	David Duchovny
	Korn
	Lee Strasberg
	Jordan
	2005 Kashmir earthquake
Brad Pitt
	Angelina Jolie
	Jennifer Aniston
	Fight Club (film)
	Rusty Ryan
	Seven (film)
	Achilles
	Plan B Entertainment
	Shawnee, Oklahoma
	Golden Globe Award
	University of Missouri
	David Fincher
	Tom Cruise
	Guy Ritchie
	Robert Redford
	ONE Campaign
	People (magazine)
	Nice
	Sudan
	CNN
	Pakistan
Johnny Depp
	Pirates of the Caribbean (film series)
	Jack Sparrow
	Edward Scissorhands
	Donnie Brasco (film)
	Kate Moss
	Academy Award for Best Actor
	Owensboro, Kentucky
	John Malkovich
	Meudon
	Anne Hathaway (actress)
	Into the Great Wide Open (song)
	The Bahamas
	Oasis (band)
	Flea (musician)
	Geffen Records
	Tom Petty
	Saint-Tropez
	New York City
	Newsweek
	Mad Love (1935 film)
Jennifer Aniston
	John Aniston
	Rachel Green
	Brad Pitt
	Friends
	Courteney Cox
	Bruce Almighty
	Office Space
	Tate Donovan
	Plan B Entertainment
	Telly Savalas
	Emmy Award
	NBC
	Ferris Bueller's Day Off
	AmeriCares
	Malibu, California
	Melissa Etheridge
	David Wain
	Burma
	Microsoft
	The Hollywood Reporter
Leonardo DiCaprio
	Inception (film)
	Titanic (1997 film)
	Frank Abagnale
	Kate Winslet
	Bar Refaeli
	Golden Globe Award for Best Actor – Motion Picture Drama
	Tobey Maguire
	Los Angeles
	Steven Spielberg
	Lower Manhattan
	Ben Kingsley
	Body of Lies (novel)
	Live Earth
	Entertainment Weekly
	Barack Obama
	Mozambique
	Germany
	Richard Yates (novelist)
	Roger Ebert
	Hod HaSharon