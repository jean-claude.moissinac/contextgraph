En français ci-après

===================================================================================

Les données de blacklist peuvent être utilisées pour éviter de surcharger des graphes de contexte ou des parcours (walks) dans ces graphes avec des données qu'on considère comme non pertinentes pour les contextes en général ou pour un contexte donné.

'regex', 'obj' et 'pred' contiennent des éléments génériques qui peuvent probablement être utilisés pour toutes sortes de contexte. 'terminalnode' est plus spécifique de la définition d'un contexte pour un domaine thématique spécifique et est donc appelé à être modifié pour une application donnée.

Le fichier extended_blacklist.json présente un exemple de version étendue de blacklist. Nous le commentons ici.

En référence aux triplets (sujet, prédicat, objet), les clés "pred" et "obj" sont utilisées.

# Clé regex
La clé "regex" peut fournir des expressions régulières qui identifient des URIs qui doivent être filtrées/éliminées du contexte.
Dans l'exemple, il y a une seule expression régulière:
```
<http:\/\/fr\.dbpedia\.org\/resource\/Modèle:.*>
```
Elle désigne un type d'URI utilisée en interne pour la structuration interne du dbpedia français, mais non porteuses d'informations spécifiques à un domaine.

# Clé pred
La clé "pred" contient une liste de prédicats qu'on ne souhaite pas prendre en compte pour un contexte.
Dans l'exemple, il y a une liste de prédicats techniques internes à dbpedia. Par exemple, le prédicat
```
"<http://dbpedia.org/ontology/wikiPageRevisionID>",
```
indique un identifiant (ID) de la révision courante de la page Wiki correspondante. De façon évidente, cela n'est pas porteur d'information sur un contexte donné.

# Clé obj
La clé "obj" contient une liste d'objets qu'on ne souhaite pas prendre en compte.
Dans l'exemple, il y a une seule URI qu'on ne veut pas voir dans les objets:
```
"<http://www.w3.org/2002/07/owl#Thing>"
```
En effet, un trop grand nombre d'entités ont ce type, qui est donc porteur d'aucune information sur les entités (ou de très peu) 

# Clé terminalnode
La clé "terminalnode" contient une liste d'objets qu'on ne souhaite considérer comme des noeuds terminaux.

Dans la phase d'extraction d'un graphe de contexte, lorsqu'on arrive sur un de ces noeuds, on ne cherche pas à aller au-delà et mettre dans le graphe des voisins de ces noeuds. Dans une phase de construction de parcours (walks), de façon analogue, on ne poursuit pas les parcours au-delà de ces noeuds.

Dans l'exemple, prenons quelques URIs considérées comme terminales.

"<http://schema.org/Place>" est utilisé pour indiquer avec le prédicat rdf:type le fait qu'une entité est un lieu. Si partant de là, on récupère -par parcours inverse- toutes les entités de ce type, on va récupérer une quantité énorme de données: tous les lieux décrits dans dbpedia. Pour certains contextes, cela peut avoir du sens. Pour d'autres, par exemple un contexte sur les musées français, cela aurait peut de sens de récupérer la description de tous les lieux du monde décrits dans dbpedia. 'terminalnode' permet d'éviter une telle situation.

"<http://www.wikidata.org/entity/Q5>" fait référence au concept de personne dans Wikidata. De façon analogue, cela s'applique à un nombre énorme d'entités. Ainsi, on a intérêt à ne pas récupérer tous les éléments liés à ce concept. On aura intérêt à plutôt jouer sur les 'germes' qui constituent les points de départ de la construction du contexte, afin d'être plus sélectif sur les personnes qu'on va inclure dans un contexte.



