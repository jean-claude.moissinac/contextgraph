#!/usr/bin/env python2
# -*- coding: UTF-8 -*-
# @Last Modified by:   vu
# @Last Modified time: 2019-01-08 20:09:20

import json
import sys
import requests
from requests.auth import HTTPDigestAuth
import urllib

def SPARQL_UPDATE(endpoint, query, cfg=None):
    update_data = {"update": query }
    if cfg and hasattr(cfg, 'auth'):
        # response = requests.post(endpoint, data=update_data, auth=HTTPDigestAuth(cfg.auth["login"], cfg.auth["pwd"]))
        response = requests.post(endpoint, data=update_data, auth=HTTPDigestAuth(cfg.auth["login"],cfg.auth["pwd"]))
    else:
        try :
            response = requests.post(endpoint, data=update_data)
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
    return response

def SPARQL_ASK(endpoint, query):
    'ask if something exist in the base'
    data = urllib.parse.urlencode({"format": u"application/sparql-results+json", "query":query}).encode('utf-8')
    try:
        req = urllib.request.Request(endpoint, data = data)
        with urllib.request.urlopen(req) as rep:
            response =  rep.read().decode('utf-8')
            if response:
                ret = json.loads(response)
                return ret["boolean"]
            else:
                return False
        return False
    except:
        print("Problem in sparql ASK")

def SPARQL_SELECT(endpoint, query):
    data = urllib.parse.urlencode( {"format": u"application/sparql-results+json", "query":query }).encode('utf-8')
    result = ""
    try:
        req = urllib.request.Request(endpoint, data=data)
        with urllib.request.urlopen(req) as rep:
            response = rep.read().decode('utf-8')
            if response:
                result = json.loads(response)
                return result
    except Exception as e:
        # raise e
        print("Problem in sparql SELECT")

if __name__ == "__main__":
    print("Debug")
    endpointFusekiSandboxUpdate = "http://localhost:3030/sandbox/update"
    sampleUpdateQuery = """prefix sb: <http://givingsense.eu/sembib/>  INSERT DATA { sb:JeanClaudeMoissinac a sb:Researcher }"""
    print(SPARQL_UPDATE(endpointFusekiSandboxUpdate, sampleUpdateQuery))



