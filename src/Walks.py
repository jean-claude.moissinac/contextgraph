# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2019-01-17 13:06:54
# @Last Modified by:   vu
# @Last Modified time: 2019-04-02 04:23:55

from Neighbors import Neighbors
from rdflib import URIRef, Literal
from rdflib.term import _is_valid_uri
import random

class Walks():
    def __init__(self):
        self.nb = Neighbors()

    def getAllWalksDepth2(self, g, node, only_uri=True):
        nb = self.nb
        neighbors = nb.getNeighbors(g, node, only_uri=only_uri)
        node = URIRef(node) if _is_valid_uri(node) else Literal(node)
        walks = [[node, item[0], item[1]] for item in neighbors]
        return walks


    def getRandomWalks(self, g, node, nb_walks, depth, prev_node=None, condition=None, only_uri=True):
        node = URIRef(node) if _is_valid_uri(node) else Literal(node)

        if depth == 0: return [[node]]

        nb = self.nb
        neighbors = nb.getNbRandomNeighbors(nb.getNeighbors(g, str(node), prev_node, condition, only_uri), nb_walks)
        if not neighbors: return [[node]]
        walks = list()
        for item in neighbors:
            sub_walks = self.getRandomWalks(
                g,
                node = str(item[1]),
                nb_walks = item[3],
                depth = depth - 2,
                prev_node = str(node),
                condition=item[2],
                only_uri=only_uri
                )
            for w in sub_walks:
                walks += [[node, item[0]] + w]
        return walks

    # return random walks + all walks at depth 2
    def getRandomWalksPlusD2(self, g, node, nb_walks, depth, prev_node=None, condition=None, only_uri=False):
        walks = self.getRandomWalks(g, node, nb_walks, depth, prev_node, condition)
        walks += self.getAllWalksDepth2(g, node, prev_node)
        return walks


    def __get_current_depth(self, to_visit):
        x = to_visit[0]
        return 2*x[2]

    def getBfsWalks(self, g, node, depth, prev_node=None, condition=None, only_uri=False, logger=None):
        root = URIRef(node)
        cur_depth = 0
        to_visit = [(root, prev_node, cur_depth)]
        walks = [[root]]
        nb = self.nb

        while to_visit and (self.__get_current_depth(to_visit) < depth):
            if logger:
                logger.info('')
                logger.info('walks: {}'.format(walks))
                logger.info('to_visit: {}'.format(to_visit))

            cur_triple = to_visit.pop(0)
            walk = walks.pop(0)

            entity = str(cur_triple[0])
            prev_node = str(cur_triple[1]) if cur_triple[1] else None
            cur_depth = cur_triple[2]

            if not isinstance(cur_triple[0], URIRef):
                walks.append(walk)
                continue
            neighbors = nb.getNeighbors(g, entity, prev_node, condition, only_uri)
            if not neighbors: walks.append(walk)
            for neighbor in neighbors:
                to_visit.append((neighbor[1], entity, cur_depth+1))
                x = list(walk)
                x.append(neighbor[0]); x.append(neighbor[1])
                walks.append(x)

        return walks

    def getBfsRandomWalks(self, g, node, nb_walks, depth, prev_node=None, condition=None, only_uri=False):
        walks = self.getBfsWalks(g, node, depth, prev_node, condition, only_uri)

        # /* sauvegader walks pour vérifier
        import pickle, os
        file_location = 'bfswalks_d{d}.walks'.format(d=depth)
        with open(file_location, "wb") as fp:
            pickle.dump(walks, fp)
        # sauvegader walks pour vérifier */

        if len(walks)<=nb_walks: return walks
        return random.sample(walks, nb_walks)


    def getBfsRandomWalksPlusD2(self, g, node, nb_walks, depth, prev_node=None, condition=None, only_uri=False):
        walks = self.getBfsRandomWalks(g, node, nb_walks, depth, prev_node, condition, only_uri)
        walks += self.getAllWalksDepth2(g, node, prev_node)
        return walks


    def getTfIdfWalks(self, g, node, nb_walks, depth, prev_node=None):
        if depth == 0: return [[node]]

        nb = self.nb
        neighbors = nb.getNbTfIdfNeighbors(nb.getTfIdfNeighbors(g, node, prev_node), nb_walks)
        if not neighbors: return [[node]]
        walks = list()
        for item in neighbors:
            sub_walks = self.getTfIdfWalks(
                g,
                node = item[1],
                nb_walks = item[2],
                depth = depth - 2,
                prev_node = node,
                )
            for w in sub_walks:
                walks += [[node, item[0]] + w]
        return walks


    @staticmethod
    def getBlacklistWalks(self, g, node, nb_walks, depth, prev_node=None):
        if depth == 0: return [[node]]

        nb = self.nb
        neighbors = nb.getNbBlacklistNeighbors(nb.getBlacklistNeighbors(g, node, prev_node), nb_walks)
        if not neighbors: return [[node]]
        walks = list()
        for item in neighbors:
            sub_walks = self.getBlacklistWalks(
                g,
                node = item[1],
                nb_walks = item[2],
                depth = depth - 2,
                prev_node = node,
                )
            for w in sub_walks:
                walks += [[node, item[0]] + w]
        return walks


def brouillon_Tuan():
    from rdflib import Graph
    g = Graph()
    file_location = 'Chuck_Norris/kga2_Cannon_Group.n3'
    g.parse(file_location, format='n3')
    node = 'http://fr.dbpedia.org/resource/Chuck_Norris'

    w = Walks()
    walks = w.getRandomWalks(g, node, 400, depth = 2)
    walks_d2 = w.getAllWalksDepth2(g, node)

if __name__ == '__main__':
    brouillon_Tuan()