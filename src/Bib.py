# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2018-11-02 01:12:56
# @Last Modified by:   vu
# @Last Modified time: 2019-03-06 11:44:54

import re
import logging, os
import unicodedata

class Bib:
    @staticmethod
    def printProgressBar(iteration, total, prefix = 'Progress:', suffix = 'Completed', decimals = 2, length = 50, fill = '█'):
        percent = ("{0:." + str(decimals) + "f}").format(100*iteration/total)
        filled_length = int(length*iteration//total)
        bar = fill*filled_length + '-'*(length-filled_length)
        print('\r%s |%s| %s%% (%d/%d)' % (prefix, bar, percent, iteration, total), end = '\r')
        if iteration == total:
            print('\r%s |%s| %s%% (%d/%d) %s' % (prefix, bar, percent, iteration, total, suffix), end = '\r')
            print()


    @staticmethod
    def isUri(url):
        regex = re.compile(
            r'^(?:http|ftp)s?://' # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
            r'localhost|' #localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
            r'(?::\d+)?' # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE
        )
        return False if re.match(regex, url) is None else True


    @staticmethod
    def setLogger(log_name, params=None):
        level = params['level'] if params and 'level' in params else logging.INFO
        saveToFile = params['saveToFile'] if params and 'saveToFile' in params else True
        isStreaming = params['isStreaming'] if params and 'isStreaming' in params else True

        logger = logging.getLogger(log_name)
        logger.setLevel(level)
        formatter = logging.Formatter('%(message)s')
        
        if isStreaming:
            streamHandler = logging.StreamHandler()
            streamHandler.setFormatter(formatter)
            logger.addHandler(streamHandler)

        if saveToFile:
            if 'log_file' not in params:
                raise Exception('Need log_file in params')
            log_dir = params['log_dir'] if 'log_dir' in params else ''
            log_file = params['log_file']
            if log_dir and not os.path.isdir(log_dir): os.mkdir(log_dir)
            log_file_location = os.path.join(log_dir, log_file)
            
            fileHandler = logging.FileHandler(log_file_location, mode='w', encoding='utf-8')
            fileHandler.setFormatter(formatter)
            logger.addHandler(fileHandler)

        return logger


    @staticmethod
    def getLogger(log_name):
        logger = logging.getLogger(log_name)
        return logger


    @staticmethod
    def removeAccents(input_str):
        """Summary
        
        Args:
            input_str (str): string to be modified
        
        Returns:
            string: string without accents
        """
        nkfd_form = unicodedata.normalize('NFKD', input_str)
        return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])