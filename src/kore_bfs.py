# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2019-03-22 15:25:22
# @Last Modified by:   vu
# @Last Modified time: 2019-03-25 11:37:21

import completeWorkflow as cwf
import kore_Uris as ku
import re, json


depth = 4 # 8
nb_walks = 21000

paramsReadUris = {
    'filename': 'kore_db_w2i_Chuck_Norris.json',
    'dir': 'data/KORE'
}
paramsLoadContext = {
    "filename": 'kga1_NorrisContext.n3',
    "contextdir": "data/graphs_0313",
}
paramsGenerateWalksD2 = {
    'only_uri': False
}
paramsSaveWalksD2 = {
    "walksdir": "data/walks/Norris",
    "graph_name": "Chuck_Norris",
    "typealgo": "WalksD2",
    "depth": 2,
    "nb_walks": 21000,
}
paramsAppendBfsWalksD4 = {
    'file_location': 'data/walks/Norris/bfswalks_d4.walks'
}
paramsAppendGeneratedRandomWalks = {
    "depth": depth,
    "nb_walks": nb_walks,
    "only_uri": False
}
paramsSaveWalksCr8Br4D2 = {
    "walksdir": "data/walks/Norris",
    "graph_name": "Chuck_Norris",
    "typealgo": "WalksCr8Br4D2",
    "depth": depth,
    "nb_walks": nb_walks
}
paramsBuildModel = {
    'methode': 'skipgram',
    # 'size': 500, # dimensionality of feature vectors
    'window': 5, # max distance between cur and predicted word within a sentence
    'worker': 4, # number of threads using
    'sg': 1, # 1: skipgram, 0: CBOW
    'hs': 0, # 1: hierarchical softmax, 0: negative sampling (if negative > 0)
    'epoch' : 10, #'iter': 10,
    'negative': 25, # between 5-20, paper: 25
    'min_count': 1
}
paramsSaveModel = {
    "model_dir": "data/walks/Norris",
    "graph_name": "graphs_Chuck_Norris_0319",
    "typealgo": "WalksCr8Br4D2",
    "depth": depth,
    "nb_walks": nb_walks,
}
paramsSpearmanr = {
    "kore_file_location": "data/KORE/Chuck_Norris.txt",
    "w2i_file_location": "data/KORE/kore_db_w2i_Chuck_Norris.json"
}

workflow = [
    {"process": ku.readUris, "params": paramsReadUris},
    {"process": cwf.loadContext, "params": paramsLoadContext},
    {"process": cwf.generateWalksD2, "params": paramsGenerateWalksD2},
    {"process": cwf.saveWalksB, "params": paramsSaveWalksD2},
    {"process": cwf.serializeWalks, "params": paramsSaveWalksD2},
    #{"process": cwf.appendWalksB, "params": paramsAppendBfsWalksD4},
    {"process": cwf.appendGeneratedRandomWalks, "params": paramsAppendGeneratedRandomWalks},
    {"process": cwf.saveWalksB, "params": paramsSaveWalksCr8Br4D2},
    {"process": cwf.serializeWalks, "params": paramsSaveWalksCr8Br4D2},
    {"process": cwf.buildModel, "params": paramsBuildModel},
    {"process": cwf.saveModel, "params": paramsSaveModel},
    {"process": ku.getSpearmanrSimilarity, "params": paramsSpearmanr},
]
res = {}
for step in workflow:
    print('Processing {}'.format(step["process"].__name__))
    res = step['process'](res, step['params'])
    if 'spearmanr' in res: print(res['spearmanr'])