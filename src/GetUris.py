# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2019-01-07 16:28:22
# @Last Modified by:   vu
# @Last Modified time: 2019-03-06 11:54:06

import json
from wordsToUris import WordsToUris

class GetUris():
    '''
    obtenir la liste d'uris des musées (paris musées) et des monuments (cmn)
    '''
    DATA_TYPES = {'cmn', 'parismusees', 'kore'}
    ERRS = {
        'dtype': 'INVALID DATA TYPE',
        'service': 'INVALID SERVICE'
    }
    URI_FIELDS = {
        'dbpedia': {
            'parismusees': 'dbpedia',
            'cmn': 'uris__http://dbpedia.org/sparql'
        },
        'frdbpedia': {
            'parismusees': 'frdbpedia',
            'cmn': 'uris__http://fr.dbpedia.org/sparql'
        },
        'wikidata': {
            'parismusees': 'wikidata',
            'cmn': 'uris__https://query.wikidata.org/sparql'
        },
        'splitter': '__'
    }
    NAME_FIELD = {
        'parismusees': 'name',
        'cmn': 'monument_nom'
    }

    '''
    get list of URI of museum ou monuments from json files
    if uri of a museum does not exist in json file
    Word2Uris will be used to find its uri based on its name
    - input:
    --- (str) file location
    --- (str) data type ('parismusees' or 'cmn')
    --- (str) service ('dbpedia', 'frdbpedia', 'wikidata')
    --- (str) endpoint: use to get uri of museum from its name
    --- (int) delay: delay time before each request to dbpedia/frdbpedia/wikidata server
    - output: (list) uris
    '''
    @staticmethod
    def get(file_location, dtype, service='frdbpedia', endpoint='http://localhost:3030/ds/query', delay=0.1, logger=None):
        if dtype not in GetUris.DATA_TYPES:
            raise ValueError(GetUris.ERRS['dtype'])
        if service not in ('dbpedia', 'frdbpedia', 'wikidata'):
            raise ValueError(GetUris.ERRS['service'])

        uris = set(); names = set()
        fields = GetUris.URI_FIELDS[service][dtype].split(GetUris.URI_FIELDS['splitter'])

        if logger: logger.info('[GetUris] file_location = {}'.format(file_location))
        with open(file_location, encoding='utf-8') as f:
            res = json.load(f)

        if logger: logger.info('[GetUris] number of items = {}'.format(len(res)))
        for data in res:
            d = data.copy()
            is_exist = True

            for field in fields:
                is_exist = is_exist and field in d and d[field]
                if is_exist:
                    d = d[field]
                else:
                    break
            if is_exist:
                uris.add(d)
            elif GetUris.NAME_FIELD[dtype] in data and \
            data[GetUris.NAME_FIELD[dtype]]:
                names.add(data[GetUris.NAME_FIELD[dtype]].strip())

        if names:
            w2u = WordsToUris()
            w2u.setQueryPoint(endpoint)
            uris = list(uris) + list(w2u.wordsToUris(names, service, delay, logger).values())

        if logger: logger.info('[GetUris] number of uris found = {}'.format(len(uris)))
        return uris


def brouillon():
    uris = GetUris.get('data/test_cmn.json', 'cmn', 'frdbpedia')
    print(len(uris))
    print(uris)

    from Bib import Bib
    logger = Bib.setLogger('test_logger', {
        'saveToFile': False,
        })

    uris = GetUris.get('source/test_parismusees.json', 'parismusees', 'dbpedia', logger=logger)
    print(uris)
    print(type(uris))
    print(len(uris))


if __name__ == '__main__':
    brouillon()