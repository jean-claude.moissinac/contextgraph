# -*- coding: UTF-8 -*-
# File: wordsToUris.py
# 11/3/2018
#  author: JC Moissinac (c) 2018
# @Last Modified by:   vu
# @Last Modified time: 2019-03-06 11:41:12
# tools to find an IRI in a Knowledge Graph which is a good association for each word in a list
# # (an IRI for each word) .

__author__ = "JC.Moissinac"
__project__ = "Data&Musée"

import logging
from writeRdfOnFuseki import SPARQL_ASK, SPARQL_SELECT
import time

class WordsToUris:
    SERVICES = {
        'dbpedia': 'http://dbpedia.org/sparql',
        'frdbpedia': 'http://fr.dbpedia.org/sparql',
        'wikidata': 'https://query.wikidata.org/sparql'
    }
    DOMAINS = {
        'dbpedia': 'dbpedia',
        'frdbpedia': 'fr.dbpedia',
        'wikidata': 'wikidata'
    }
    ERRS = {
        'service': 'INVALID SERVICE',
        'querypoint': 'INVALID QUERY POINT'
    }

    def __init__(self):
        try:  # Python 2.7+
            from logging import NullHandler
        except:
            class NullHandler(logging.Handler):
                def emit(self, record):
                    pass
        logging.getLogger(__name__).addHandler(NullHandler)
        self.querypoint = None
        self.service = None
        self.sparqlTemplate = '''   
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            select distinct ?iri where {
              service <__service> {
                {
                  ?iri rdfs:label "__word"
                } UNION {
                  ?iri rdfs:label "__word"@fr
                } UNION {
                  ?iri rdfs:label "__word"@en
                }
              }
            }
        ''' #default query is a sample useful with dbpedia
        self.setDisambiguate(self.domainDisambiguate)

    def defaultDisambiguate(self, choices):
        # default: choose nothing
        return None

    def simpleDisambiguate(self, choices):
        # simple: choose the first possibility
        return choices[0]

    def domainDisambiguate(self, choices):
        choices = list(choices)
        if not choices: return None

        domain = self.DOMAINS[self.service]
        values = [x for x in choices if 'resource' in x and domain in x and not 'Category' in x and not 'Cat\u00e9gorie' in x]
        if values:
            return values[0]
        else:
            values = [x for x in choices if domain in x]
            return values[0] if values else choices[0]

    def setSparqlTemplate(self, template):
        previousTemplate = self.sparqlTemplate
        self.sparqlTemplate = template
        return previousTemplate

    def setDisambiguate(self, fct):
        self.disambiguate = fct

    def setQueryPoint(self, querypoint):
        self.querypoint = querypoint

    def wordToUri(self, word, service='frdbpedia', delay=1):
        if service not in self.SERVICES:
            raise ValueError(self.ERRS['service'])
        if not self.querypoint:
            raise ValueError(self.ERRS['querypoint'])

        self.service = service
        time.sleep(delay) # solution provisoire simple pour le délai
        wordVariants = [word, word.lower(), word.upper(), word.title()]
        # ajouter passage au singulier des pluriels et vice-versa?
        '''des exemples non trouvés dans wikidata:
        tag clouds, alors que tag cloud existe https://www.wikidata.org/wiki/Q263864
        Fibroblasts alors que fibroblast existe https://www.wikidata.org/wiki/Q463418
        Routers -> router -> https://www.wikidata.org/wiki/Q5318
        '''
        # ajouter majuscule seulement sur la première lettre ex: Instant messaging
        choices = list()
        sparqlRequest = self.sparqlTemplate.replace("__service", self.SERVICES[service])
        for tstword in wordVariants:
            tstword = tstword.replace('"', '\\"')
            query = sparqlRequest.replace("__word", tstword)
            res = SPARQL_SELECT(self.querypoint, query)
            if res and ("results" in res) and ("bindings" in res["results"]):
                choices += [x["iri"]["value"] for x in res["results"]["bindings"]]
        iri = self.disambiguate(choices)
        return iri

    def callBuilder(self, endpoint, uri, prop, depth, format, process):
        call = {}
        call["endpoint"] = self.querypoint
        call["callback"] = process
        query = '''
        '''
        call["params"] = {"query": query, "format": format}
        return call

    def wordsToUris(self, wordsList, service='frdbpedia', delay=1, logger=None):
        if logger: logger.info('[WordsToUris] number of words = {}'.format(len(wordsList)))

        irisList = {}; missed_uris = list()
        for word in wordsList:
            iri = self.wordToUri(word, service, delay)
            if iri:
                irisList[word] = iri
            else:
                missed_uris.append(word)
        if logger: logger.info('[WordsToUris] missed_uris = {}'.format(missed_uris))
        return irisList

if __name__ == '__main__':
    endpoint = 'http://localhost:3030/ds/query'
    w2i = WordsToUris()
    w2i.setQueryPoint(endpoint)
    print(w2i.wordToUri("Xbox 360", 'dbpedia'))
    print(w2i.wordToUri('Templeton "Faceman" Peck', delay=0.1))
    print(w2i.wordToUri("Tour Pey-Berland", 'dbpedia', delay=0.1))
    print(w2i.wordsToUris(['Xbox 360', "Tour Pey-Berland"], delay=0.1))

    from Bib import Bib
    logger = Bib.setLogger('test_logger', {
        'saveToFile': False,
        })
    print(w2i.wordsToUris(['Templeton "Faceman" Peck', "Javascript", "javascript", "Python", "Musée Carnavalet"], delay=0.1, logger=logger))
