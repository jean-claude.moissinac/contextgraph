# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2019-01-17 11:09:58
# @Last Modified by:   vu
# @Last Modified time: 2019-04-01 17:08:30

from rdflib import URIRef, BNode, Literal
from rdflib.term import _is_valid_uri
import math, random

class Neighbors():
	def __init__(self):
		self.total_entities = None
		self.cached_entities_pred = dict()
		self.cached_preds_entity = dict()
		self.cached_tf_idf = dict()
		self.cached_neighbors = dict()
		self.PRED_BLACKLIST = [
			# 'http://dbpedia.org/ontology/wikiPageWikiLink',
		]

	def getNeighbors(self, g, entity, prev_node=None, condition=None, only_uri=False):
		"""Summary
		Get all not BNode neighbors of an entity in a context graph

		Args:
		    g (Graph): context graph
		    entity (str): entity is to look for its neighbors
		    prev_node (None, optional): previous node of entity in the walks
		    	it is not counted in neighbors
		    condition (None, optional): for furture feature
		    only_uri (bool, optional):
		    	- True: get only uri neighbors
		    	- False: get both uri neighbors and literal neighbors

		Returns:
		    LIST: list of triples (predicate, neighbor, condition)
		"""
		if entity in self.cached_neighbors: return self.cached_neighbors[entity]
		neighbors = set()
		condition = "givingsense"

		# if not _is_valid_uri(entity): return neighbors
		# node = URIRef(entity)
		node = URIRef(entity) if _is_valid_uri(entity) else Literal(entity)


		for row in g.predicate_objects(node):
			if str(row[1]) == prev_node: continue
			if only_uri:
				if isinstance(row[1], URIRef):
					if not isinstance(row[1], BNode):
						neighbors.add((row[0], row[1], condition))
			else:
				if not isinstance(row[1], BNode):
					neighbors.add((row[0], row[1], condition))

		for row in g.subject_predicates(node):
			if str(row[0]) == prev_node: continue
			if only_uri:
				if isinstance(row[0], URIRef):
					if not isinstance(row[0], BNode):
						neighbors.add((row[1], row[0], condition))
			else:
				if not isinstance(row[0], BNode):
					neighbors.add((row[1], row[0], condition))

		self.cached_neighbors[entity] = neighbors
		return neighbors


	def getNbRandomNeighbors(self, neighbors, nb_walks):
		"""Summary
		Allocate number of walks to neighbors

		Args:
		    neighbors (list): list of neighbors
		    nb_walks (int): number of walks

		Returns:
		    LIST: list of neighbors with number of walks on it
		"""
		if not neighbors: return list()

		nb_neighbors = list()
		sub_nbw = math.floor(nb_walks/len(neighbors))
		residual = nb_walks % len(neighbors)

		neighbors4res = random.sample(list(neighbors), residual) # add set to list conversion
		nb_neighbors += [x + (sub_nbw + 1,) for x in neighbors4res]
		if sub_nbw > 0:
			nb_neighbors += [x + (sub_nbw,) for x in neighbors if x not in neighbors4res]

		return nb_neighbors


	'''
	Tf Idf
	'''
	def getTotalEntities(self, g):
		if self.total_entities is not None: return self.total_entities

		entities = set()
		for s in g.subjects(): entities.add(s)
		for o in g.objects(): entities.add(o)
		self.total_entities = len(entities)
		return self.total_entities

	def getEntitiesPred(self, g, pred):
		if self.cached_entities_pred and pred in self.cached_entities_pred:
			return self.cached_entities_pred[pred]

		entities = set()
		for s in g.subjects(predicate=URIRef(pred)): entities.add(s)
		for o in g.objects(predicate=URIRef(pred)): entities.add(s)
		self.cached_entities_pred[pred] = len(entities)
		return self.cached_entities_pred[pred]

	def getPredsEntity(self, g, entity):
		if self.cached_preds_entity and entity in self.cached_preds_entity:
			return self.cached_preds_entity[entity]

		nb_preds = 0
		for p in g.predicates(subject=URIRef(entity)): nb_preds += 1
		for p in g.predicates(object=URIRef(entity)): nb_preds += 1
		self.cached_preds_entity[entity] = nb_preds
		return self.cached_preds_entity[entity]

	def countPredEntity(self, g, pred, entity):
		nb_preds = 0
		for o in g.objects(subject=URIRef(entity), predicate=URIRef(pred)): nb_preds += 1
		for s in g.objects(object=URIRef(entity), predicate=URIRef(pred)): nb_preds += 1
		return nb_preds

	def getTfIdf(self, g, pred, entity):
		if self.cached_tf_idf and entity in self.cached_tf_idf \
			and pred in self.cached_tf_idf[entity]: return self.cached_tf_idf[entity][pred]

		tf = self.countPredEntity(g, pred, entity)/self.getPredsEntity(g, entity)
		idf = math.log(self.getTotalEntities(g)/self.getEntitiesPred(g, pred))
		tf_idf = tf*idf

		if self.cached_tf_idf:
			if entity not in self.cached_tf_idf:
				self.cached_tf_idf[entity] = dict()
				self.cached_tf_idf[entity][pred] = tf_idf
			else:
				self.cached_tf_idf[entity][pred] = tf_idf
		else:
			self.cached_tf_idf[entity] = dict()
			self.cached_tf_idf[entity][pred] = tf_idf
		return self.cached_tf_idf[entity][pred]

	def getTfIdfNeighbors(self, g, entity, prev_node=None):
		neighbors = self.getNeighbors(g, entity, prev_node)
		tfidf_neighbors = list()
		for pred, neighbor in neighbors:
			weight = self.getTfIdf(g, pred, entity)
			tfidf_neighbors.append((pred, neighbor, weight))
		return tfidf_neighbors


	def getNbTfIdfNeighbors(self, tfidf_neighbors, nb_walks):
		if not tfidf_neighbors: return list()
		preds = dict()
		nei = dict()
		total = 0
		for neighbor in tfidf_neighbors:
			pred = neighbor[0]
			weight = neighbor[2]
			if pred not in nei:
				nei[pred] = list()
				nei[pred].append(neighbor)
			else:
				nei[pred].append(neighbor)

			if pred not in preds:
				preds[pred] = weight
				total += weight

		for pred in preds: preds[pred] = math.ceil(nb_walks*preds[pred]/total)
		nb_neighbors = list()
		for neighbor in tfidf_neighbors:
			pred = neighbor[0]
			nb = preds[pred]
			if nb < len(nei[pred]):
				# get first nb of nei[pred], each has nb = 1
				nb_neighbors += [(pred, x, 1) for x in nei[pred][:nb]]
			else:
				nb_neighbors += [(pred, x, math.ceil(nb/len(nei[pred]))) for x in nei[pred][:nb]]
		return nb_neighbors


	'''
	black list
	'''
	def getBlacklistNeighbors(self, g, entity, prev_node=None):
		neighbors = self.getNeighbors(g, entity, prev_node)
		blacklist_neighbors = list()
		for pred, neighbor in neighbors:
			if pred in self.PRED_BLACKLIST:
				weight = 0.5
			else:
				weight = 1
			blacklist_neighbors.append((pred, neighbor, weight))
		return blacklist_neighbors


	def getNbBlacklistNeighbors(self, blacklist_neighbors, nb_walks):
		if not blacklist_neighbors: return list()
		preds = dict()
		nei = dict()
		total = 0
		for neighbor in blacklist_neighbors:
			pred = neighbor[0]
			weight = neighbor[2]
			if pred not in nei:
				nei[pred] = list()
				nei[pred].append(neighbor)
			else:
				nei[pred].append(neighbor)

			if pred not in preds:
				preds[pred] = weight
				total += weight

		for pred in preds: preds[pred] = math.ceil(nb_walks*preds[pred]/total)
		nb_neighbors = list()
		for neighbor in blacklist_neighbors:
			pred = neighbor[0]
			nb = preds[pred]
			if nb < len(nei[pred]):
				# get first nb of nei[pred], each has nb = 1
				nb_neighbors += [(pred, x, 1) for x in nei[pred][:nb]]
			else:
				nb_neighbors += [(pred, x, math.ceil(nb/len(nei[pred]))) for x in nei[pred][:nb]]
		return nb_neighbors