# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2018-11-02 00:20:44
# @Last Modified by:   vu
# @Last Modified time: 2019-03-12 15:42:36

from SparqlTools import SparqlTools

class GraphTools:
	__queries_template = {
		'ask': '''
			ASK {
				__graph { __triple }
			}
		''',
		'update': '''
			INSERT DATA{
				__graph { __triple }
			}
		''',
		'remove': '''
			DELETE DATA{
				__graph { __triple }
			}
		'''
	}

	def __init__(self, graph=None):
		self.graph = graph

	def setGraph(self, graph):
		self.graph = graph

	def getGraph(self):
		return self.graph


	def select(self, query, service):
		try:
			res = SparqlTools.select(service, query)
			return res['results']['bindings'] if res else None
		except Exception as e:
			raise e


	def askForTriple(self, triple, service):
		graph = "GRAPH <" + self.graph + ">" if self.graph else ""

		query = self.__queries_template['ask'].replace('__graph', graph)
		query = query.replace('__triple', triple)
		try:
			return SparqlTools.ask(service, query)
		except Exception as e:
			raise e


	def addTriple(self, triple, endpoint):
		graph = "GRAPH <" + self.graph + ">" if self.graph else ""
		
		query = self.__queries_template['update'].replace('__graph', graph)
		query = query.replace('__triple', triple)
		try:
			SparqlTools.update(endpoint, query)
		except Exception as e:
			raise e


	def removeTriple(self, triple, endpoint):
		strGraph = "GRAPH <" + self.graph + ">" if self.graph else ""

		query = self.__queries_template['remove'].replace('__graph', strGraph)
		query = query.replace('__triple', triple)
		try:
			SparqlTools.update(endpoint, query)
		except Exception as e:
			raise e