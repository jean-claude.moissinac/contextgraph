# -*- coding: utf-8 -*-
# @Author: vu
# @Date:   2019-01-13 20:48:02
# @Last Modified by:   vu
# @Last Modified time: 2019-03-13 15:05:33

from KGAspiratorD1 import KGAspiratorD1
from Bib import Bib
from rdflib import Graph, URIRef

class BuildContext():
    def __init__(self, service='frdbpedia', depth=2):
        self.service = service
        self.depth = depth
        self.aspirator = None

    def setService(self, service):
        self.service = service

    def setDepth(self, depth):
        self.depth = depth


    def getUris(self, graph):
        uris = set()
        for s, p, o in graph:
            if isinstance(s, URIRef): uris.add(s.n3().strip('<').strip('>'))
            if isinstance(o, URIRef): uris.add(o.n3().strip('<').strip('>'))
        return list(uris)

    def build(self, uri, params):
        blacklist = params['blacklist'] if 'blacklist' in params else None
        delay = params['delay'] if 'delay' in params else 0.1
        show_progress = params['show_progress'] if 'show_progress' in params else True
        follow_classes = params['follow_classes'] if 'follow_classes' in params else False
        get_classes = params['get_classes'] if 'get_classes' in params else False
        graph = params['graph'] if 'graph' in params else None
        logger = params['logger'] if 'logger' in params else None
        prev_graph = params['prev_graph'] if 'prev_graph' in params else None

        if logger: logger.info('[BuildContext] uri = {}'.format(uri))
        if show_progress:
            print('-- Building context of uri: {}'.format(uri))
        g = graph if graph else Graph()
        if self.aspirator:
            kga = self.aspirator
        else:
            kga = KGAspiratorD1(blacklist = blacklist, delay = delay)
            self.aspirator = kga

        followed_uris = set()
        typed_uris = set()
        for d in range(self.depth):
            if d == 0:
                g = kga.getExcerpt(g, uri, self.service, logger=logger)
                followed_uris.add(uri)
                g = kga.addTypes(g, uri, self.service, logger=logger)
                typed_uris.add(uri)

            if d > 0 and g:
                uris = self.getUris(g)
                l = len(uris)
                if show_progress:
                    Bib.printProgressBar(0, l, prefix='-- Progress:')
                for i, uri in enumerate(uris):
                    if not uri in followed_uris:
                        g = kga.getExcerpt(g, uri, self.service, follow_classes, logger)
                        followed_uris.add(uri)
                    if not uri in typed_uris:
                        g = kga.addTypes(g, uri, self.service, follow_classes, logger)
                        typed_uris.add(uri)
                    if show_progress:
                        Bib.printProgressBar(i+1, l, prefix='-- Progress:')
            # add the types for all new uris
            if get_classes:
                uris = self.getUris(g)
                for uri in uris:
                    if not uri in typed_uris:
                        g = kga.addTypes(g, uri, self.service, logger=logger)
                        typed_uris.add(uri)
        return g


def brouillon():
    blacklist = {
        'pred': [
            '<http://www.w3.org/2000/01/rdf-schema#comment>',
            '<http://www.w3.org/2002/07/owl#sameAs>',
            '<http://dbpedia.org/ontology/abstract>',
            '<http://fr.dbpedia.org/property/visiteurs>'
        ],
        'obj': [
            '<http://www.w3.org/2002/07/owl#Thing>',
            '<http://www.w3.org/2003/01/geo/wgs84_pos#SpatialThing>',
        ],
        'terminalnode': [
        ],
        'regex': [
            '(<http:\/\/fr\.dbpedia\.org\/resource\/Modèle:.*>)',
        ]
    }

    uri = 'http://fr.dbpedia.org/resource/Arc_de_triomphe'
    service = 'http://fr.dbpedia.org/sparql'
    depth = 1

    params = {
        "blacklist": blacklist,
        "delay": 0.1,
        "show_progress": False,
        "follow_classes": False,
        "get_classes": False,
        "logger": None,
        'prev_graph': None
    }

    bc = BuildContext(service, depth)
    g = bc.build(uri, params)
    print(g.serialize(format='n3'))

if __name__ == '__main__':
    brouillon()