# ContextGraph



## Introduction

One challenge in utilizing knowledge graphs, especially with machine learning techniques, is the issue of scalability. In this context, we propose a method to substantially reduce the size of these graphs, allowing us to concentrate on the most relevant sections of the graph for a specific application or context. We define the notion of context graph as an extract from one or more general knowledge bases (such as DBpedia, Wikidata, Yago) that contains the set of information relevant to a specific domain while preserving the properties of the original graph. We validate the approach on a DBpedia excerpt for entities related to the Data\&Musée project and the KORE reference set according to two aspects: the coverage of the context graph and the preservation of the similarity between its entities. The results show that the use of context graphs makes the exploitation of large knowledge bases more manageable and efficient while preserving the features of the initial graph. 

The code and data below allow us to reproduce some of our experiments. 

## Structure

The src directory contains the source code and a data sub-directory with useful data to run the experiments.

The main code is in the completeWorkflow.py file. It is used to build a context graph (CG) and walks in the CG. Then a model using the walks is built.

Much of the documentation is in the code. Here we introduce the principles of implementation 

In completeWorkflow.py, the workflow variable is used to define the steps in the workflow. Each step in the workflow complete, modify and returns a 'res' structure used by the following step. 

A sample workflow is:
```    
workflow = [
        {"process": buildUriList,  "params": paramsGetUris},
        {"process": contextBuilder, "params": paramsContextBuilder},
        # {"process": loadContext, "params": paramsLoadContext},
        # {"process": loadGraph, "params": paramsLoadGraph },
        # # generateRandomWalks or generateTfIdfWalks or generateBlacklistWalks
        {"process": generateRandomWalks, "params": paramsGenerateWalks},
        {"process": saveWalks, "params": paramsSaveWalks},
        # {"process": loadWalks, "params": paramsSaveWalks},
        {"process": buildModel, "params": paramsBuildModel},
        {"process": saveModel, "params": paramsSaveModel},
    ]
```

The steps are designed so that you can restart the process at any point without having to do it all over again. For example, if a CG has already been built, construction steps can be skipped and replaced by a loadGraph step.

In the workflow list, each "process" key define the function to execute, each "params" key define a structure which defines the parameters of the function. 
In the list of workflows, each "process" key defines the function to be executed, and each "params" key defines a structure that defines the function parameters. The parameters are commented out with the corresponding function.